import unittest
import subprocess

import lman
import lisp2lman


class TestEncode(unittest.TestCase):
    def testEncodeList(self):
        self.assertEqual(
            lisp2lman.encode([1,2,3,4]),
            '(cons 1 (cons 2 (cons 3 (cons 4 0))))'
        )
        self.assertEqual(
            lisp2lman.encode([]),
            '0'
        )

    def testEncodeTuple(self):
      self.assertEqual(
          lisp2lman.encode((1,2,3,4)),
          '(cons 1 (cons 2 (cons 3 4)))'
      )


vitality = 0
lman_pos = (100, 500)
direction = 2
lives = 1
score = 9001
lman_status = (vitality, lman_pos, direction, lives, score)
a_map = ['000', '022']
world_state = (a_map, lman_status, 0, 0)
world = lisp2lman.encode(world_state)


class LispTest(unittest.TestCase):

    def setUp(self):
        self.lman = lman.Lman()
        # Push some garbage onto data stack for main args.
        self.lman._PUSH_s((lman.TAG_INT, 5))
        self.lman._PUSH_s((lman.TAG_INT, 6))

    def _runCode(self, code, input_int):
        self.lman._PUSH_s((lman.TAG_INT, input_int))
        self.lman._PUSH_s((lman.TAG_INT, input_int))
        lman_code = lisp2lman.lisp2lman(code)
        #print '\n'.join('% 3d %s' % (i, l) for (i, l) in enumerate(lman_code.splitlines()))
        p = subprocess.Popen(['pbcopy'], stdin=subprocess.PIPE)
        p.communicate(lman_code)
        try:
          return self.lman.exec_gcc(lman_code.splitlines())
        except:
          #print lman_code
          print '\n'.join('% 3d %s' % (i, l) for (i, l) in enumerate(lman_code.splitlines()))
          raise

    def _testPiece(self, piece, libs=[]):
      code = """((lambda (arg1 arg2) %(piece)s) %(libs)s)""" % {
              'piece': piece,
              'libs': '\n'.join('#include %s ' % l for l in libs),
              }

      try:
          stack = self._runCode(code, 1)
      except Exception:
          print code
          raise
      return stack[0]

    def _assertList(self, res, expected_list):
        lst = []
        current = res
        while not current == (lman.TAG_INT, 0):
            self.assertEqual(res[0], lman.TAG_CONS)
            cons = self.lman.conss[current[1]]
            lst.append(cons[0])
            current = cons[1]

        self.assertEqual(lst, expected_list)

    def testCond(self):
        res = self._testPiece("(cond (= 5 5) 1 0)")
        self.assertEqual(res, (lman.TAG_INT, 1))

        self.lman.__init__()
        res = self._testPiece("(cond (= 5 6) 1 0)")
        self.assertEqual(res, (lman.TAG_INT, 0))

    def testIsAtom(self):
        piece = "(is_atom %s)"

        res = self._testPiece(piece % '5')
        self.assertEqual(res, (lman.TAG_INT, 1))

        self.lman.__init__()
        res = self._testPiece(piece % '(cons 3 4)')
        self.assertEqual(res, (lman.TAG_INT, 0))

    def testCons(self):
        piece = "(cons 3 4)"
        res = self._testPiece(piece)
        self.assertEqual(res[0], lman.TAG_CONS)
        self.assertEqual(self.lman.conss[res[1]], (
          (lman.TAG_INT, 3),
          (lman.TAG_INT, 4)
        ))

    def testCar(self):
        piece = "(car (cons 3 4))"
        res = self._testPiece(piece)
        self.assertEqual(res, (lman.TAG_INT, 3))

    def testCdr(self):
        piece = "(cdr (cons 3 4))"
        res = self._testPiece(piece)
        self.assertEqual(res, (lman.TAG_INT, 4))

    def testLet(self):
        piece = "(let ((x (+ 1 arg1))) (let ((y (+ 1 x))) (+ x y)))"
        res = self._testPiece(piece)
        self.assertEqual(res, (lman.TAG_INT, 5))

    def testT_elem(self):
        test_cases = [
            # n, is_last, result.
            (0, 0, 1),
            (1, 0, 2),
            (2, 1, 3),
        ]

        libs = ['tuples.lisp']
        for t in test_cases:
            piece = "(t_elem (cons 1 (cons 2 3)) %d %d)" % (t[0], t[1])
            res = self._testPiece(piece, libs)
            self.assertEqual(res, (lman.TAG_INT, t[2]))
            self.lman.__init__()

    def testListLen(self):
        test_cases = [
            (0, 0),
            ("(cons 1 0)", 1),
            ("(cons 5 (cons 7 0))", 2),
        ]

        libs = ['lists.lisp']
        for t in test_cases:
            piece = "(length %s)" % t[0]
            res = self._testPiece(piece, libs)
            self.assertEqual(res, (lman.TAG_INT, t[1]))
            self.lman.__init__()

    def testL_elem(self):
        test_cases = [
            # n, is_last, result.
            (0, 1),
            (1, 2),
            (2, 3),
        ]

        libs = ['lists.lisp']
        for t in test_cases:
            piece = "(l_elem (cons 1 (cons 2 (cons 3 0))) %d)" % t[0]
            res = self._testPiece(piece, libs)
            self.assertEqual(res, (lman.TAG_INT, t[1]))
            self.lman.__init__()

    def testMapRowsCols(self):
        libs = ['world.lisp']
        piece = "(world_map_rows %s)" % world
        res = self._testPiece(piece, libs)
        self.assertEqual(res, (lman.TAG_INT, 2))

        self.lman.__init__()
        piece = "(world_map_cols %s)" % world
        res = self._testPiece(piece, libs)
        self.assertEqual(res, (lman.TAG_INT, 3))

    def testMapGetCell(self):
        libs = ['world.lisp']
        piece = "(get_map_cell %s %%d %%d)" % world
        test_cases = [
            (0, 0, 0),
            (0, 2, 0),
            (1, 0, 0),
            (1, 2, 2),
        ]
        for t in test_cases:
            res = self._testPiece(piece % (t[1], t[0]), libs)
            self.assertEqual(res, (lman.TAG_INT, t[2]))
            self.lman.__init__()

    def testLmanPos(self):
        libs = ['world.lisp']
        piece = "(get_lman_pos %s)" % world

        res = self._testPiece(piece, libs)

        self.assertEqual(res[0], lman.TAG_CONS)
        cons = self.lman.conss[res[1]]
        self.assertEqual(cons, ((lman.TAG_INT, lman_pos[0]),
                                (lman.TAG_INT, lman_pos[1])))

    def testLmanVit(self):
        libs = ['world.lisp']
        piece = "(get_lman_vitality %s)" % world

        res = self._testPiece(piece, libs)

        self.assertEqual(res, (lman.TAG_INT, vitality))

    def testL_min(self):
        libs = ['lists.lisp']
        piece = "(l_min %s)"
        test_cases = [
            [1,2,3],
            [1,2,1],
            [1,1,1],
            [3,2,1],
            [5,3,4],
        ]
        for t in test_cases:
            res = self._testPiece(piece % lisp2lman.encode(t), libs)
            self.assertEqual(res, (lman.TAG_INT, min(t)))
            self.lman.__init__()

    def testL_filter(self):
        libs = ['lists.lisp']
        piece = '(filter (lambda (x) (> x 0)) (cons 0 (cons 1 (cons 2 0))))'

        res = self._testPiece(piece, libs)
        self._assertList(res, [(lman.TAG_INT, 1),
                               (lman.TAG_INT, 2)])

    def testL_map(self):
        libs = ['lists.lisp']

        piece = '''(l_map (lambda (x) (+ x 3)) (cons 0 (cons 1 (cons 2 0))))'''

        res = self._testPiece(piece, libs)
        self._assertList(res, [(lman.TAG_INT, 3),
                               (lman.TAG_INT, 4),
                               (lman.TAG_INT, 5)])

    def testL_lookup(self):
        lookup_list = [(1,2), (4,5), (6,7)]

        libs = ['lists.lisp']
        piece = '(lookup %s %%d)' % (lisp2lman.encode(lookup_list))

        test_cases = [
            (1, [(lman.TAG_INT, 2)]),
            (4, [(lman.TAG_INT, 5)]),
            (2, []),
        ]
        for t in test_cases:
            res = self._testPiece(piece % t[0], libs)
            self._assertList(res, t[1])
            self.lman.__init__()

    def testL_add_to_list(self):
        libs = ['lists.lisp']
        piece = '(lookup (add_to_list (add_to_list (add_to_list 0 6 7) 4 5) 1 2) %d)'

        test_cases = [
            (1, [(lman.TAG_INT, 2)]),
            (4, [(lman.TAG_INT, 5)]),
            (2, []),
        ]
        for t in test_cases:
            res = self._testPiece(piece % t[0], libs)
            self._assertList(res, t[1])
            self.lman.__init__()

    def testL_del_from_list(self):
        libs = ['lists.lisp']
        lookup_list = [(1,2), (4,5), (6,7)]
        lookup_piece = '(lookup %s %%d)' % lisp2lman.encode(lookup_list)
        del_piece = '(lookup (del_from_list %s %%d) %%d)' % lisp2lman.encode(lookup_list)

        test_cases = [
            # key -> result
            (1, [(lman.TAG_INT, 2)]),
            (4, [(lman.TAG_INT, 5)]),
            (2, []),
        ]
        for t in test_cases:
            res = self._testPiece(lookup_piece % t[0], libs)
            self._assertList(res, t[1])
            self.lman.__init__()

        del_test_cases = [
            # del key -> lookup key -> result
            (1, 1, []),
            (2, 1, [(lman.TAG_INT, 2)]),
            (4, 1, [(lman.TAG_INT, 2)]),
            (1, 4, [(lman.TAG_INT, 5)]),
            (1, 2, []),
            (2, 2, []),
        ]
        for t in del_test_cases:
            res = self._testPiece(del_piece % (t[0], t[1]), libs)
            self._assertList(res, t[2])
            self.lman.__init__()

    def testL_min_list_value(self):
        libs = ['lists.lisp']
        test_cases = [
          ([(1,2), (4,5), (6,7)], (lman.TAG_INT, 2)),
          ([], (lman.TAG_INT, 9999)),
          ([(1,1), (2, 1)], (lman.TAG_INT, 1)),
        ]
        for t in test_cases:
            piece = '(min_list_value %s)' % lisp2lman.encode(t[0])

            res = self._testPiece(piece, libs)
            self.assertEqual(res, t[1])
            self.lman.__init__()

    #def test_a_star(self):
    #    libs = ['a-star.lisp']
    #    a_map = [[2, 2, 0],
    #             [2, 2, 2]]

    #    piece_tmpl = '(a-star %s %%s %%s)' % lisp2lman.encode(a_map)

    #    test_cases = [
    #            # start, goal, path
    #            #((0,0), (0,0), [(0,0)]),
    #            ((0,0), (2,1), []),

    #    ]
    #    for t in test_cases:
    #        piece = piece_tmpl % tuple(map(lisp2lman.encode, t[:2]))
    #        res = self._testPiece(piece, libs)
    #        path = lman.decode_list(self.lman.conss, res)
    #        self.assertEqual(len(path), len(t[2]))
    #        for p_elem, expected in zip(path, t[2]):
    #            self.assertEqual(p_elem[0], lman.TAG_CONS)
    #            pair = self.lman.conss[p_elem[1]]
    #            self.assertEqual(pair[0][1], expected[0])
    #            self.assertEqual(pair[1][1], expected[1])
    #        self.lman.__init__()

    def testLambda(self):
        libs = []
        test_cases = [
            ('((lambda (x) (+ x 2)) 5) )', 7),
            ('((lambda () 5))', 5),
        ]

        for t in test_cases:
            res = self._testPiece(t[0], libs)
            self.assertEqual(res, (lman.TAG_INT, t[1]))
            self.lman.__init__()

        # As the last one but with no extra (), so lambda is returned and not executed.
        res = self._testPiece('(lambda () 5)', libs)
        self.assertEqual(res[0], lman.TAG_CLOSURE)


    def testNodeOnMap(self):
        libs = ['world.lisp']

        a_map = [[0,0,0],
                 [0,0,0]]
        piece = '(node_on_map %s %%s)' % lisp2lman.encode(a_map)

        test_cases = [
                ((0, 0), 1),
                #((1, 1), 1),
                #((2, 1), 1),
                #((3, 0), 0),
                #((0, 3), 0),
                #((-1, 0), 0),
                #((0, -1), 0),
        ]

        for t in test_cases:
            res = self._testPiece(piece % lisp2lman.encode(t[0]), libs)
            self.assertEqual(res, (lman.TAG_INT, t[1]))
            self.lman.__init__()

    def testLetFilter(self):
        libs = ['lists.lisp']
        piece = '(let ((p (lambda (x) (> x arg1)))) (filter p (cons 0 (cons 9 0))))'

        res = self._testPiece(piece, libs)
        self._assertList(res, [(lman.TAG_INT, 9)])
        self.lman.__init__()

    def testArgmin(self):
        lookup_list = [(1,2), (4,5), (6,1)]

        libs = ['lists.lisp']
        piece = '(argmin %%s %s)' % (lisp2lman.encode(lookup_list))

        test_cases = [
            ('(lambda (x) (car x))', ((lman.TAG_INT, 1), (lman.TAG_INT, 2))),
            ('(lambda (x) (+ 2 (car x)))', ((lman.TAG_INT, 1), (lman.TAG_INT, 2))),
            ('(lambda (x) (+ (car x) (cdr x)))', ((lman.TAG_INT, 1), (lman.TAG_INT, 2))),
            ('(lambda (x) (/ (cdr x) (car x)))', ((lman.TAG_INT, 6), (lman.TAG_INT, 1))),
        ]
        for t in test_cases:
            res = self._testPiece(piece % t[0], libs)
            cons = self.lman.conss[res[1]]
            self.assertEqual(cons, t[1])
            self.lman.__init__()

    #def testNeighborNodes(self):
    #    libs = ['a-star.lisp']

    #    a_map = [[0,0,0],
    #             [0,0,0]]
    #    piece = '(neighbor_nodes %s %%s)' % lisp2lman.encode(a_map)

    #    test_cases = [
    #            ((0, 0), [(1, 0), (0, 1)]),
    #            ((1, 1), [(1, 0), (2, 1), (0, 1)]),
    #            ((2, 1), [(2, 0), (1, 1)]),
    #    ]

    #    for t in test_cases:
    #        res = self._testPiece(piece % lisp2lman.encode(t[0]), libs)
    #        l = lman.decode_list(self.lman.conss, res)
    #        self.assertEqual(len(l), len(t[1]))
    #        for cons_ref, expected in zip(l, t[1]):
    #            self.assertEqual(cons_ref[0], lman.TAG_CONS)
    #            pair = self.lman.conss[cons_ref[1]]
    #            self.assertEqual(pair[0][1], expected[0])
    #            self.assertEqual(pair[1][1], expected[1])
    #        self.lman.__init__()

    def testL_find(self):
        lookup_list = [1,2,3,1,4,1]

        libs = ['lists.lisp']
        piece = '(l_find %s %%d)' % (lisp2lman.encode(lookup_list))

        test_cases = [
            (1, [(lman.TAG_INT, 5),
                 (lman.TAG_INT, 3),
                 (lman.TAG_INT, 0)]),
        ]
        for t in test_cases:
            res = self._testPiece(piece % t[0], libs)
            self._assertList(res, t[1])
            self.lman.__init__()

    def testGetLocationsByType(self):
        libs = ['world.lisp']

        a_map = [[1,2,1, 3],
                 [0,0,4, 3],
        ]
        piece = '(get_locations_by_type %s %%s)' % lisp2lman.encode(a_map)

        test_cases = [
                (0, [(1,1),(0,1)]),
                (1, [(2,0),(0,0)]),
                (2, [(1,0)]),
                (3, [(3,0), (3,1)]),
                (4, [(2,1)]),
        ]

        for t in test_cases:
            res = self._testPiece(piece % t[0], libs)
            poss = lman.decode_list(self.lman.conss, res)
            self.assertEqual(len(poss), len(t[1]))
            for cons, expected in zip(poss, t[1]):
                pair = self.lman.conss[cons[1]]
                self.assertEqual(pair[0][1], expected[0])
                self.assertEqual(pair[1][1], expected[1])
            self.lman.__init__()

    def testDirection(self):
        libs = ['world.lisp']

        piece = '(direction %s %%s)' % lisp2lman.encode((1,1))

        test_cases = [
                ((0, 1), 3),
                ((1, 0), 0),
                ((2, 1), 1),
                ((1, 2), 2),
        ]

        for t in test_cases:
            res = self._testPiece(piece % lisp2lman.encode(t[0]), libs)
            self.assertEqual(res, (lman.TAG_INT, t[1]))
            self.lman.__init__()

    def testL_concat(self):
        libs = ['lists.lisp']

        piece = '(l_concat %s %s)' % (lisp2lman.encode([(1,2)]),
                                      lisp2lman.encode([(3,4)]))
        expected = [(1,2), (3,4)]

        res = self._testPiece(piece, libs)

        items = lman.decode_list(self.lman.conss, res)
        self.assertEqual(len(items), len(expected))
        for cons, expected in zip(items, expected):
            pair = self.lman.conss[cons[1]]
            self.assertEqual(pair[0][1], expected[0])
            self.assertEqual(pair[1][1], expected[1])
        self.lman.__init__()

    def testL_reduce(self):
        libs = ['lists.lisp']

        piece = '(l_reduce (lambda (x y) (cons x y)) %s 0)' % (
                lisp2lman.encode([2,3,4]))

        res = self._testPiece(piece, libs)

        self._assertList(res, [
            (lman.TAG_INT, 2),
            (lman.TAG_INT, 3),
            (lman.TAG_INT, 4),
        ])
        #self.assertEqual(res, (lman.TAG_INT, 24))


if __name__ == '__main__':
    unittest.main()
