#!/bin/bash

set -x 

SOLUTION_PROGRAM="samples/main.lisp"
SOLUTION_GHOST_PROGRAM="lame_examples/lame_bot_2.lm"

version=$(date '+%Y%m%d-%H%M%S')
root="solutions/${version}"

mkdir -p $root/{code,solution}

python2 lisp2lman.py < ${SOLUTION_PROGRAM} > $root/solution/lambdaman.gcc
python2 lame2ghc.py -f ${SOLUTION_GHOST_PROGRAM} > $root/solution/ghost0.ghc
cp *.py $root/code
cp -R samples $root/code
cp -R lame_examples $root/code 
cp build_solution.sh $root/code 

cd $root
archive_name="../solutions-${version}.tar.gz"
tar czvf $archive_name *
sha1sum $archive_name

