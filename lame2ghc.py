# @a, @b, ..., @h - registers
#
# $some_value, $a, $foo_bar_24 - variables
#
# if expr {
#   branch1
# } else {
#   branch2
# }



import collections
import re
import sets
import logging
from optparse import OptionParser

from sexpparser import sexp

INDENT = '  '


ARITHMETIC_OP_MAP = {
    "+=": "add",
    "-=": "sub",
    "*=": "mul",
    "/=": "div",
    "^=": "xor",
    "&=": "and",
    "|=": "or",
}

logging.basicConfig(format='%(message)s')
log = logging.getLogger(__name__)

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  default="/dev/stdin",
                  help="read program from FILE", metavar="FILE")
(options, args) = parser.parse_args()


class Parser(object):

    def __init__(self, source):
        self.source = source
        self.lines = map(lambda x: x.strip(), source.split("\n"))

        self.variable_dict = {} # variable name -> address
        self.occupied_addresses = sets.Set()

    def lame2ghc(self):
        result = self.parse(self.lines, -1)
        result.append("hlt")
        return "\n".join(result)
        #return "\n".join(map(lambda (i, x): "%-3s: %s" % (i+1, x), enumerate(result)))

    def parse(self, lines, instr_shift):
        result = []

        is_condition = False
        is_loop = False
        brace_balance = 0
        acc = []
        for line in lines:
            if not line:
                continue
            if is_condition:
                acc.append(line)
                brace_balance += line.count("{") - line.count("}")
                if brace_balance == 0:
                    is_condition = False
                    result += self.parse_condition(acc, instr_shift + len(result))
                    acc = []
            elif is_loop:
                acc.append(line)
                brace_balance += line.count("{") - line.count("}")
                if brace_balance == 0:
                    is_loop = False
                    result += self.parse_loop(acc, instr_shift + len(result))
                    acc = []
            elif self.is_condition_start(line):
                is_condition = True
                brace_balance += line.count("{") - line.count("}")
                acc.append(line)
            elif self.is_loop_start(line):
                is_loop = True
                brace_balance += line.count("{") - line.count("}")
                acc.append(line)
            elif self.is_assignment(line):
                result += self.parse_assignment(line)
            elif self.is_arithmetic_op(line):
                result += self.parse_arithmetic_op(line)
            elif self.is_interrupt(line):
                result += self.parse_interrupt(line)

        return result

    def is_interrupt(self, line):
        return line.strip().startswith("%")

    def parse_interrupt(self, line):
        m = re.match("%(\\w+)\\s*\\((.*)\\)", line)
        interrupt_name = m.group(1)
        args = m.group(2)

        if interrupt_name == "get_ghost_index":  # args: (variable)
            l_value = self.evaluate_l_value(args)
            return ["int 3", "mov %s,a" % l_value]
        elif interrupt_name == "set_ghost_direction":  # args: (variable)
            l_value = self.evaluate_l_value(args)
            return ["mov a,%s" % l_value, "int 0"]
        elif interrupt_name == "get_lman_position":  # args: (lman_index, x, y), e.g. (1, $x, $y)
            args_split = map(lambda x: x.strip(), args.split(","))
            assert len(args_split) == 3
            lman_index = self.evaluate_r_value(args_split[0])
            x_var = self.evaluate_l_value(args_split[1])
            y_var = self.evaluate_l_value(args_split[2])
            return ["int %s" % lman_index, "mov %s,a" % x_var, "mov %s,b" % y_var]
        elif interrupt_name == "get_ghost_start_position":  # args: (ghost_index, x, y), e.g. (1, $x, $y)
            args_split = map(lambda x: x.strip(), args.split(","))
            assert len(args_split) == 3
            ghost_index = self.evaluate_r_value(args_split[0])
            x_var = self.evaluate_l_value(args_split[1])
            y_var = self.evaluate_l_value(args_split[2])
            return ["mov a,%s" % ghost_index, "int 4", "mov %s,a" % x_var, "mov %s,b" % y_var]
        elif interrupt_name == "get_ghost_current_position":  # args: (ghost_index, x, y), e.g. (1, $x, $y)
            args_split = map(lambda x: x.strip(), args.split(","))
            assert len(args_split) == 3
            ghost_index = self.evaluate_r_value(args_split[0])
            x_var = self.evaluate_l_value(args_split[1])
            y_var = self.evaluate_l_value(args_split[2])
            return ["mov a,%s" % ghost_index, "int 5", "mov %s,a" % x_var, "mov %s,b" % y_var]
        elif interrupt_name == "get_ghost_info":  # args: (ghost_index, vitality, direction)
            args_split = map(lambda x: x.strip(), args.split(","))
            assert len(args_split) == 3
            ghost_index = self.evaluate_r_value(args_split[0])
            vitality = self.evaluate_l_value(args_split[1])
            direction = self.evaluate_l_value(args_split[2])
            return ["mov a,%s" % ghost_index, "int 6", "mov %s,a" % vitality, "mov %s,b" % direction]
        elif interrupt_name == "get_map_square_info":  # args: (x, y, map_info)
            args_split = map(lambda x: x.strip(), args.split(","))
            assert len(args_split) == 3
            x = self.evaluate_r_value(args_split[0])
            y = self.evaluate_r_value(args_split[1])
            map_info = self.evaluate_l_value(args_split[2])
            return ["mov a,%s" % x, "mov b,%s" % y, "int 7", "mov %s,a" % map_info]
        elif interrupt_name == "trace":
            return ["int 8"]

    def is_loop_start(self, line):
        return line.strip().startswith("while ") and line.strip().endswith("{")

    def parse_loop(self, acc, instr_shift):
        result = []
        condition_token = re.match("while (.*){", acc[0]).group(1)
        l_value, op_token, r_value = self.parse_condition_token(condition_token)

        body = acc[1:-1]

        body_parsed = self.parse(body, instr_shift + 2)
        last_body_instr = instr_shift + len(body_parsed) + 3

        result.append("%s %s,%s,%s" % (op_token, instr_shift + 3, l_value, r_value))
        result.append("jeq %s,0,0" % (last_body_instr + 1))
        result += body_parsed
        result.append("jeq %s,0,0" % (instr_shift + 1))

        return result

    def is_condition_start(self, line):
        return line.strip().startswith("if ") and line.strip().endswith("{")

    def parse_condition(self, acc, instr_shift):
        result = []
        condition_token = re.match("if (.*){", acc[0]).group(1)
        l_value, op_token, r_value = self.parse_condition_token(condition_token)
        else_start = None
        brace_balance = 0
        for i, l in enumerate(acc):
            brace_balance += l.count("{") - l.count("}")
            if brace_balance == 1 and re.match("\\s*}\\s+else\\s+{\\s*", l):
                else_start = i
                break

        if else_start is None:
            if_part = acc[1:-1]
            else_part = None
        else:
            if_part = acc[1:else_start]
            else_part = acc[else_start + 1:-1]

        if_part_parsed = self.parse(if_part, instr_shift + 2)
        last_if_part_instr = instr_shift + len(if_part_parsed) + 3
        else_part_parsed = self.parse(else_part, last_if_part_instr) if else_part is not None else None
        last_cond_instr = instr_shift + len(if_part_parsed) + 3 + (0 if else_part is None else len(else_part_parsed))

        result.append("%s %s,%s,%s" % (op_token, instr_shift + 3, l_value, r_value))
        result.append("jeq %s,0,0" % (last_if_part_instr + 1))
        result += if_part_parsed
        result.append("jeq %s,0,0" % (last_cond_instr + 1))
        if else_part_parsed is not None:
            result += else_part_parsed


        return result

    def parse_condition_token(self, condition_token):
        if condition_token.find("<") != -1:
            op = "jlt"
            split_on = "<"
        elif condition_token.find(">") != -1:
            op = "jgt"
            split_on = ">"
        else:
            op = "jeq"
            split_on = "=="

        l_value_token, r_value_token = map(lambda x: x.strip(), condition_token.split(split_on))
        l_value = self.evaluate_r_value(l_value_token)
        r_value = self.evaluate_r_value(r_value_token)
        return l_value, op, r_value

    def is_assignment(self, line):
        return re.match("[^<>=+\\-*/^&|]+=[^<>=]+", line)

    def parse_assignment(self, line):
        l_value_token, r_value_token = map(lambda x: x.strip(), line.split("="))
        l_value = self.evaluate_l_value(l_value_token)
        r_value = self.evaluate_r_value(r_value_token)

        instr = "mov %s,%s" % (l_value, r_value)
        return [instr]

    def is_arithmetic_op(self, line):
        return re.match("[^<>=]+[+\\-*/^&|]=[^<>=]+", line)

    def parse_arithmetic_op(self, line):
        eq_index = line.find("=")
        op = line[eq_index-1:eq_index+1]

        l_value_token, r_value_token = map(lambda x: x.strip(), line.split(op))
        l_value = self.evaluate_l_value(l_value_token)
        r_value = self.evaluate_r_value(r_value_token)

        instr = "%s %s,%s" % (ARITHMETIC_OP_MAP[op], l_value, r_value)
        return [instr]

    def evaluate_r_value(self, token):
        if self.is_register(token):
            return token[1:]
        elif self.is_variable(token):
            return self.variable_content(token)
        elif self.is_number(token):
            return token
            
    def evaluate_l_value(self, token):
        if self.is_register(token):
            return token[1:]
        elif self.is_variable(token):
            return self.variable_content(token)
        elif self.is_number(token):
            return token

    def is_register(self, token):
        return re.match("@[a-h]", token.strip()) is not None

    def is_variable(self, token):
        return re.match("\\$\\w+", token.strip()) is not None

    def is_number(self, token):
        is_number = re.match("\\d+", token.strip())
        if is_number:
            number = int(token)
            assert 0 <= number < 256

        return is_number

    def variable_content(self, token):
        var_name = token[1:]
        if var_name in self.variable_dict:
            var_address = self.variable_dict[var_name]
        else:
            var_address = self.find_unoccupied_var_address()
            self.variable_dict[var_name] = var_address
            self.occupied_addresses.add(var_address)
        return "[%s]" % var_address


    def find_unoccupied_var_address(self):
        for i in xrange(0, 256):
            if i not in self.occupied_addresses:
                return i

def main():
    source = open(options.filename).read()
    print Parser(source).lame2ghc() 

if __name__ == "__main__":
    main()

