"""Sample invocation: python lisp2lman.py -f samples/sexp.lisp."""
import collections
import logging
from optparse import OptionParser

from sexpparser import sexp

INDENT = '  '


logging.basicConfig(format='%(message)s')
log = logging.getLogger(__name__)

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  default="/dev/stdin",
                  help="read program from FILE", metavar="FILE")
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
(options, args) = parser.parse_args()


Function = collections.namedtuple("Function", "name contents")

class Environment(object):
    def __init__(self):
        self.branches = {}
        self.branch_addrs = {}
        self.functions = {}
        self.func_addrs = {}
        self.vars = [{}]

    def lookup_var(self, var_name):
        for level, env in enumerate(self.vars):
            if var_name in env:
                return (level, env[var_name])
        return None

    def lookup_func(self, func_name):
        return self.func_addrs.get(func_name)

    def lookup_branch(self, br_name):
        return self.branch_addrs.get(br_name)

    def push_env_frame(self):
        self.vars.insert(0, {})

    def pop_env_frame(self):
        self.vars.pop(0)

def trampoline(env, exec_step=False):
    """exec_step - start execution with step functionm not main"""
    func_defs = env.functions.values()
    content_len = sum(len(f.contents) for f in func_defs)
    header_len = 3
    header = [
      ["LDC", 1],
      ["SEL", header_len + content_len, 0],
      ["RTN", '\t\t; final RTN'],          # final return
    ]
    if not exec_step:
        footer = [
            ["DUM", len(func_defs), '\t; trampoline'],
        ]
    else:
        footer = [
            ["DUM", 2]
        ]

    addr = header_len
    for func_def in func_defs:
        env.func_addrs[func_def.name] = addr
        footer.append(["LDF", addr, '\t; decl ' + func_def.name])
        addr += len(func_def.contents)

    if not exec_step:
        footer.extend([
            ["LDF", env.func_addrs['main'], '\t; prepare main to be called'],
            ["RAP", len(func_defs)],
            ["JOIN"],
            ])
    else:
        footer.extend([
            ["LDC", 5],
            ["LDC", 5],
            ["LDF", env.func_addrs['step'], '\t; prepare main to be called'],
            ["RAP", 2],
            ["JOIN"],
            ])
    return header, footer


_SEL_ID = 0


def t(exp, env):
    """Returns list of (INSTRUCTION, ARG1, ARG2)."""
    global _SEL_ID
    if isinstance(exp, basestring):
        lookup = env.lookup_var(exp)
        if lookup is not None:
            return [['LD', lookup[0], lookup[1]]]

    binary_ops = {
            '+': 'ADD',
            '-': 'SUB',
            '*': 'MUL',
            '/': 'DIV',
            '=': 'CEQ',
            '>': 'CGT',
    }

    if isinstance(exp, list) and len(exp) == 1:
        exp = exp[0]

    if isinstance(exp, int):
        return [['LDC', exp]]

    if exp[0] == 'defun':
        f_name = exp[1]
        args_names = exp[2]
        local_env = env.vars[0]
        for i, name in enumerate(args_names):
            local_env[name] = i
        env.push_env_frame()
        body = t(exp[3].asList(), env)
        env.pop_env_frame()
        env.functions[f_name] = Function(f_name, [
            ['DUM', len(args_names), '\t; def ' + f_name]
        ] + body + [
            ['RTN', '\t\t; ret from ' + f_name]
        ])
        return []
    elif exp[0] in binary_ops:
        assert len(exp) == 3
        arg1 = t(exp[1], env)
        arg2 = t(exp[2], env)
        return arg1 + arg2 + [[binary_ops[exp[0]]]]
    elif exp[0] == 'cons':
        assert len(exp) == 3
        arg1 = t(exp[1], env)
        arg2 = t(exp[2], env)
        return arg1 + arg2 + [['CONS']]
    elif exp[0] == 'car':
        assert len(exp) == 2
        arg = t(exp[1], env)
        return arg + [["CAR"]]
    elif exp[0] == 'cdr':
        assert len(exp) == 2
        arg = t(exp[1], env)
        return arg + [["CDR"]]
    elif exp[0] == 'cond':
        assert len(exp) == 4
        cond = t(exp[1], env)
        select_id = _SEL_ID
        _SEL_ID += 1
        true_name = "%d_true" % select_id
        false_name = "%d_false" % select_id
        true = t(exp[2], env) + [["JOIN"]]
        false = t(exp[3], env) + [["JOIN"]]
        env.branches[true_name] = true
        env.branches[false_name] = false
        return cond + [
                ["SEL", true_name, false_name]
        ]
    elif exp[0] == 'is_atom':
        return t(exp[1], env) + [["ATOM"]]
    elif exp[0] == 'let':
        assert len(exp) == 3
        var_decls = exp[1]
        value = exp[2]

        for var_decl in var_decls:
            var_name = var_decl[0]
            var_value = t(var_decl[1], env)

            env.functions[var_name] = Function(var_name, [
                ['DUM', 0, '\t; let ' + var_name]
            ] + var_value + [
                ['RTN', '\t\t; ret from let ' + var_name]
            ])

        return t(value, env)
    elif exp[0] == 'func_ref':
        return [["LDF", exp[1]]]
    if isinstance(exp, basestring): # just a func name (from let). apply it.
        return [
                ["DUM", 0],
                ["LDF", exp],
                ["RAP", 0],
        ]

    # Func application.
    res = []
    f_name = exp[0]
    f_args = exp[1:]
    res.append(["DUM", len(f_args), '\t; frame for %s application' % f_name])
    env.push_env_frame()

    for arg in f_args:
        res.extend(t(arg, env))
    env.pop_env_frame()
    res.append(["LDF", f_name, '\t; load function %s' % f_name])
    res.append(["RAP", len(f_args), "\t; call %s" % f_name])
    return res


def transform_top(s):
    exp = sexp.parseString(s)

    env = Environment()

    for top_level_fun in exp[0]:
        code = t(top_level_fun, env)
        assert not code  # top level must func defs which don't execute anything themselves.

    tramp_hdr, tramp_footer = trampoline(env)

    result = tramp_hdr + sum([f.contents for f in env.functions.values()], []) + tramp_footer
    addr = len(result)
    for br_name, branch in env.branches.items():
        env.branch_addrs[br_name] = addr
        result += branch
        result[addr] += ['\t; branch %s' % br_name]
        addr += len(branch)
    return env, result


def render(env, instructions):
    res = []
    for instr in instructions:
        if instr[0] == 'LDF':
            try:
                int(instr[1])
            except ValueError:
                instr.append('\t; call ' + instr[1])
                instr[1] = env.lookup_func(instr[1])
        elif instr[0] == 'SEL':
            try:
                int(instr[1])
                int(instr[2])
            except ValueError:
                instr[1] = env.lookup_branch(instr[1])
                instr[2] = env.lookup_branch(instr[2])
        line = ' '.join(map(str, instr)) + '\n'
        if ':' not in line:
            line = INDENT + line
        res.append(line)

    return ''.join(res)

def main():
    s = open(options.filename).read()

    env, instructions = transform_top(s)
    print render(env, instructions)

if __name__ == '__main__':
    main()
