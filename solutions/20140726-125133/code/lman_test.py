import unittest
import lman


def binary_op_test(method_name, arg1, arg2, result):
    def inner(self):
        self.lman._PUSH_s((lman.TAG_INT, arg1))
        self.lman._PUSH_s((lman.TAG_INT, arg2))
        getattr(self.lman, method_name)()
        actual_res = self.lman._POP_s()
        self.assertEqual(actual_res[0], lman.TAG_INT)
        self.assertEqual(actual_res[1], result)
    return inner


class LmanTest(unittest.TestCase):
    def setUp(self):
        self.lman = lman.Lman()

    def testExecLocalGcc(self):
        inp = open('samples/local.gcc').readlines()

        self.assertEqual(lman.exec_gcc(inp), [(lman.TAG_INT, 42)])

    def testExecConsGcc(self):
        inp = open('samples/down.gcc').readlines()

        self.assertEqual(lman.exec_gcc(inp), [(lman.TAG_CONS, 0)])

    testAdd0 = binary_op_test('ADD', 1, 2, 3)
    testAdd1 = binary_op_test('ADD', 10, 2, 12)
    testSub0 = binary_op_test('SUB', 15, 4, 11)
    testSub1 = binary_op_test('SUB', 5, 14, -9)
    testDiv0 = binary_op_test('DIV', 15, 4, 3)
    testDiv1 = binary_op_test('DIV', 15, 5, 3)
    testDiv2 = binary_op_test('DIV', 15, 6, 2)
    testCeq0 = binary_op_test('CEQ', 15, 6, 0)
    testCeq1 = binary_op_test('CEQ', 15, 15, 1)
    testCgt0 = binary_op_test('CGT', 15, 15, 0)
    testCgt1 = binary_op_test('CGT', 16, 15, 1)
    testCgt2 = binary_op_test('CGT', 14, 15, 0)
    testCgte0 = binary_op_test('CGTE', 15, 15, 1)
    testCgte1 = binary_op_test('CGTE', 16, 15, 1)
    testCgte2 = binary_op_test('CGTE', 14, 15, 0)

    def testDum(self):
        orig_env_frames = self.lman.env_frames

        self.lman.DUM(5)

        self.assertEqual(len(self.lman.env_frames), len(orig_env_frames) + 1)
        self.assertEqual(len(self.lman.env_frames[0]), 5)

    def testSEL(self):
        program = """
        LDC %d
        SEL 3 5
        RTN
        LDC 75
        JOIN
        LDC 76
        JOIN
        """
        cases = [(0, 76), (1, 75)]
        for value, expected in cases:
            self.assertEqual(lman.exec_gcc((program%value).splitlines()),
                             [(lman.TAG_INT, expected)])


if __name__ == '__main__':
    unittest.main()
