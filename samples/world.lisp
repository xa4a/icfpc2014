#include a-star.lisp
#include lists.lisp
(defun get_map (world_state) (t_elem world_state 0 0))
(defun get_lman_status (world_state) (t_elem world_state 1 0))
(defun get_ghosts_status (world_state) (t_elem world_state 2 0))
(defun get_fruit_status (world_state) (t_elem world_state 3 1))

(defun world_map_rows (world_state) (length (get_map world_state)))
(defun world_map_cols (world_state) (length (l_elem (get_map world_state) 0)))
(defun map_rows (map) (length map))
(defun map_cols (map) (length (car map)))
(defun get_map_cell (world_state x y) (l_elem (l_elem (get_map world_state) y) x))
(defun map_cell (map x y) (l_elem (l_elem map y) x))

(defun get_lman_vitality (world_state) (t_elem (get_lman_status world_state) 0 0))
(defun get_lman_pos (world_state) (t_elem (get_lman_status world_state) 1 0))
(defun get_lman_direction (world_state) (t_elem (get_lman_status world_state) 2 0))
(defun get_lman_lives (world_state) (t_elem (get_lman_status world_state) 3 0))
(defun get_lman_score (world_state) (t_elem (get_lman_status world_state) 4 1))

(defun mod (num den) (cond (< num den)
                           num
                           (mod (- num den) den)
                     )
)

(defun find_goals (world) 
  (cond (get_fruit_status world)
        (get_fruit_location (get_map world))
        (l_concat 
                  (cons (closest_of 
                    (get_pill_locations
                     (get_map world))
                    (get_lman_pos world)) 0)
                  (cons (closest_of 
                    (get_powerpill_locations 
                      (get_map world))
                    (get_lman_pos world)) 0)
        )
  )
)

(defun closest_of (goals current) 
  (argmin (lambda (x) (heuristic x current)) goals)
)

(defun get_fruit_location (map) 
  (get_locations_by_type map 4)
)

(defun get_powerpill_locations (map) 
  (get_locations_by_type map 3)
)

(defun get_pill_locations (map) 
  (get_locations_by_type map 2)
)

(defun get_locations_by_type (map type)
  (get_locations_by_type2 map type 0)
)
(defun get_locations_by_type2 (map type row)
  (cond (is_atom map)
        0
        (let (
              (this_row (l_map (lambda (x) 
                                 (cons x row))
                               (l_find (car map) type))
              ) 
             )
          (l_concat this_row 
                    (get_locations_by_type2 (cdr map)
                                            type
                                            (+ 1 row)
                    )
          )
        )
   )
)

(defun l_neighbour_up (cell)
  (cons (car cell) (- (cdr cell) 1))
)

(defun l_neighbour_right (cell)
  (cons (+ 1 (car cell)) (cdr cell))
)

(defun l_neighbour_down (cell)
  (cons (car cell) (+ (cdr cell) 1))
)

(defun l_neighbour_left (cell)
  (cons (- (car cell) 1) (cdr cell))
)

(defun l_neighbour (world cell direction)
  (let (
        (neigh_cell
          (cond (= direction 0)
            (l_neighbour_up cell)
            (cond (= direction 1)
                  (l_neighbour_right cell)
                  (cond (= direction 2)
                        (l_neighbour_down cell)
                        (l_neighbour_left cell)
                  )
            )
          )
        )
        )
    (get_map_cell world (car neigh_cell) (cdr neigh_cell))
  )
)

(defun direction (from to) 
  (let
    (
     (dx (- (car to) (car from)))
     (dy (- (cdr to) (cdr from)))
     )
    (cond (* (= dx 0) (> dy 0))
          2
          (cond (* (= dx 0) (< dy 0))
                0
                (cond (* (> dx 0) (= dy 0))
                      1
                      3
                 )
           )
     )
  )
)

(defun overlay_ghosts (world_state) 
  (
   overlay_ghosts2 
   (get_map world_state) 
   (get_ghosts_status world_state)
  )
)
(defun overlay_ghosts2 (map ghosts)
  (cond (is_atom ghosts)
        map
        (let
          (
           (ghost_loc (t_elem (car ghosts) 1 0))
          )
          (
           overlay_ghosts2 
           (update_map map ghost_loc 7)
           (cdr ghosts)
          )  
        )
  )
)

(defun t_update (tuple pos value)
  (cond (= pos 0)
        (cons value (cdr tuple))
        (cons (car tuple) (t_update (cdr tuple) (- pos 1) value))
  )
)

(defun update_map (map position new_value)
  (cond (= 0 (cdr position))
        (cons (t_update (car map) 
                        (car position) 
                        new_value)
              (cdr map)
        )
        (cons (car map)
              (update_map (cdr map) 
                          (cons (car position) 
                                (- (cdr position) 1)
                          )
                          new_value
              )
        )
  )
)
