(defun get_state_open (t) (t_elem t 0 0))
(defun get_state_closed (t) (t_elem t 1 0))
(defun get_state_came_from (t) (t_elem t 2 0))
(defun get_state_g_score (t) (t_elem t 3 0))
(defun get_state_f_score (t) (t_elem t 4 0))
(defun get_state_goal (t) (t_elem t 5 1))
(defun make_state (open closed came_from g_score f_score goal)
    (cons open (cons closed (cons came_from (cons g_score (cons f_score goal)))))
)

(defun abs (x) (cond (< x 0) (- 0 x) x))

(defun heuristic (start goal) 
  (* 2 (+ (abs (- (car start) (car goal))) (abs (- (cdr start) (cdr goal)))))
)
(defun distance (map current neighbor) 
  (let (
        (cell (map_cell map (car neighbor) (cdr neighbor)))
       )
    (cond (= cell 0)
           200
           (cond (= cell 6)
                 120
                 (cond (= cell 7)
                       150
                       1
                 )
           )
    )
  )
)

(defun a-star (map start goal)
  (let (
      (open (cons (cons start 1) 0))
      (closed 0)
      (came_from 0) 
      (g_score (cons (cons start 0) 0))
      (f_score 0)
    )
    (let (
          (init_state (make_state open closed came_from g_score f_score goal))
          )
     (let (
           (res (a-star2 map init_state goal))
           )
       (reconstruct_path (get_state_came_from res) goal)
     )
    )
  )
)

(defun a-star2 (map state goal) 
  (let
    (
     (open (get_state_open state))
     (closed (get_state_closed state))
    )
    (cond (length open)
      (let (
            (current (car (argmin 
                            (lambda (pair) 
                              (car (lookup (get_state_f_score state) 
                                      (car pair)
                                   )
                              )
                            )
                            open
                           )
                     )
            )
           )
        (cond (t_eq current goal)
              state
              (let (
                    (open2 (del_from_list open current))
                    (closed2 (add_to_list closed current 1))
                    (neighbors (neighbor_nodes map current))
                    )
                (let (
                      (new_state (t_update (t_update state 1 closed2) 0 open2))
                     )
                     (
                      a-star2 map (eval_neighbors map new_state current neighbors) goal
                     )
                )
              )
        )
      )
      0
    )
  )
)

(defun eval_neighbors (map state current neighbors)
  (cond (is_atom neighbors)
        state
        (let 
         (
          (neighbor (car neighbors))
          )
         (cond (is_atom (lookup (get_state_closed state) neighbor))
               (let
                 (
                  (tentative_g_score (+
                                       (car (lookup (get_state_g_score state) current))
                                       (distance map current neighbor)
                                       )
                                     )
                  (neighbor_in_open (- 1 (is_atom (lookup (get_state_g_score state) neighbor)))
                  )
                 )
                 (cond (+ (- 1 neighbor_in_open)                          
                          (cond neighbor_in_open
                                (< tentative_g_score (car (lookup (get_state_g_score state) neighbor)))
                                0
                          )
                       )
                       (let
                         (
                          (came_from2 (add_to_list (get_state_came_from state) neighbor current))
                          (g_score2 (add_to_list (get_state_g_score state) neighbor tentative_g_score))
                          (open2 (cond (= 0 (lookup (get_state_open state) neighbor))
                                       (add_to_list (get_state_open state) neighbor 1)
                                       (get_state_open open)
                                       )
                                 )
                          )
                         (let (
                               (f_score2 (add_to_list (get_state_f_score state) 
                                                      neighbor
                                                      (+ (car (lookup g_score2 neighbor))
                                                         (heuristic neighbor 
                                                                    (get_state_goal state))
                                                      )
                                         )
                               )
                           )
                           (let (
                                 (new_state (t_update (t_update (t_update (t_update state 
                                                                                    0 open2) 
                                                                          4 f_score2) 
                                                                3 g_score2) 
                                                      2 came_from2))
                                ) 
                                (eval_neighbors map new_state current (cdr neighbors))
                           )
                          )
                         )
                       (eval_neighbors map state current (cdr neighbors))
                       )
                 )
               (eval_neighbors map state current (cdr neighbors))
               )
         )
  )
)

(defun reconstruct_path (came_from current)
  (cond (is_atom (lookup came_from current))
        (cons current 0)
        (cons current (reconstruct_path came_from (car (lookup came_from current))))
  )
)

(defun in_range (value min_v max_v)
  (* (< value max_v)
     (<= min_v value)
  )
)

(defun node_on_map (map node)
  (* (in_range (car node) 0 (map_cols map))
     (in_range (cdr node) 0 (map_rows map))
  )
)

(defun neighbor_nodes (map current)
  (let 
    (
     (all_neighbors (cons (l_neighbour_up current) 
                         (cons (l_neighbour_right current)
                               (cons (l_neighbour_down current)
                                     (cons (l_neighbour_left current) 0)
                               )
                         )
                   )
     )
    )
    (filter (lambda (n) (* (node_on_map map n)
                           (- 1 (bad_cell (map_cell map (car n) (cdr n)))
                        ))) all_neighbors)
  )
)
(defun bad_cell (type) 
  (+
    (= type 0)
    (+ (= type 6)
       (= type 7)
    )
  )
)
