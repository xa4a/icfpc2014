#include world.lisp

    (defun wall_on_right (world_state) 
      (let (
            (right_direction (mod (+ 1 (get_lman_direction world_state)) 4))
           )
           (= (l_neighbour world_state (get_lman_pos world_state) right_direction)
              0)
      )
    )
    (defun wall_in_front (world_state)
           (= (l_neighbour world_state 
                           (get_lman_pos world_state)
                           (get_lman_direction world_state))
              0)
    )
    (defun wall_on_left (world_state)
      (let (
            (left_direction (mod (+ 3 (get_lman_direction world_state)) 4))
           )
           (= (l_neighbour world_state (get_lman_pos world_state) left_direction)
              0)
      )
    )

