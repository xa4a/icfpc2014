(
 #include utils.lisp
    (defun main (world_state undoc) (cons 0
                                          (func_ref step)))
    (defun step (ai_state world_state) 
      (cons ai_state
            (cond (wall_on_left world_state)
                  (cond (wall_in_front world_state)
                    (cond (wall_on_right world_state) 
                          (mod (+ 2 (get_lman_direction world_state)) 4)
                          (mod (+ 1 (get_lman_direction world_state)) 4)
                    )
                    (get_lman_direction world_state)
                  )
                  (mod (+ 3 (get_lman_direction world_state)) 4)
            )
      )
    )
)
