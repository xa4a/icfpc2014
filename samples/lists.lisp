#include tuples.lisp

(defun length (list) (cond (is_atom list)
                           0
                           (+ (length (cdr list)) 1)
                     )
)
(defun l_elem (list n) (cond (= n 0)
                             (car list)
                             (l_elem (cdr list) (- n 1))
                        )
)

(defun l_min (list) (l_min2 list 9999))
(defun l_min2 (list curr_min) (cond (is_atom list)
                             curr_min
                             (l_min2 (cdr list) (cond (< (car list) curr_min)
                                                      (car list)
                                                      curr_min
                                                )
                             )
))

(defun filter (predicate list) (cond (is_atom list)
                                     0
                                     (cond (predicate (car list))
                                               (cons (car list) (filter predicate (cdr list)))
                                               (filter predicate (cdr list))
                                               )
                                )
)

(defun l_map (map_f list) (cond (is_atom list)
                              0
                              (cons (map_f (car list)) (l_map map_f (cdr list)))
                        )
)

(defun lookup (list key) 
  (cond (is_atom list)
        0
        (let
         (
          (head (car list))
          (tail (cdr list))
         )
         (cond 
           (t_eq (car head) key)
           (cons (cdr head) 0)
           (lookup tail key)
         )
        )
  )
)
(defun add_to_list (list key value)
  (cond (is_atom list)
        (cons (cons key value) 0)
        (let
         (
          (head (car list))
          (tail (cdr list))
         )
         (cond (t_eq (car head) key)
               (cons (cons key value) tail)
               (cons head (add_to_list tail key value))
         )
        )
  )
)
(defun del_from_list (list key)
  (cond (is_atom list)
        list
        (let
         (
          (head (car list))
          (tail (cdr list))
         )
         (cond (t_eq (car head) key)
               tail
               (cons head (del_from_list tail key))
         )
        )
  )
)

(defun min_list_value (list)
    (min_list_value2 list 9999)
)
(defun min_list_value2 (list curr_min) 
  (cond (is_atom list)
        curr_min
        (let (
              (next_min (cond (< (cdr (car list)) curr_min)
                              (cdr (car list))
                              curr_min)
              )
             )
          (min_list_value2 (cdr list) next_min)
        )
  )
)
(defun argmin (mapper list) 
  (argmin2 mapper (cdr list) (car list)))
(defun argmin2 (mapper list curr_min)
  (cond (is_atom list)
        curr_min
        (argmin2 mapper
                 (cdr list) 
                 (cond (< (mapper (car list)) (mapper curr_min))
                       (car list)
                       curr_min
                 )
        )
  )
)
(defun l_find (list elem)
  (l_find2 list elem 0 0)
)
(defun l_find2 (list elem pos acc)
  (cond (is_atom list)
        acc
        (l_find2 (cdr list) elem (+ 1 pos) 
          (cond (= (car list) elem)
                (cons pos acc)
                acc
          )
        )
   )
)
(defun l_concat (l1 l2) 
  (cond (is_atom l1)
        l2
        (cons (car l1) (l_concat (cdr l1) l2))
  )
)

(defun reverse (list) 
  (reverse2 list 0)
)
(defun reverse2 (list acc)
  (cond (is_atom list)
        acc
        (reverse2 (cdr list) (cons (car list) acc))
  )
)

(defun l_reduce (func list acc)
  (cond (is_atom list)
        acc
        (func (car list) (l_reduce func (cdr list) acc))
  )
)
