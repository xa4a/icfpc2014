(defun bad_cell (type) 
  (+
    (= type 0)
    (+ (= type 6)
       (= type 7)
    )
  )
)
(defun good_cell (type)
  (+ (= type 2)
     (+ (= type 3)
        (= type 4)
     )
  )
)

(defun bfs (map start)
  (let 
    (
     (queue (cons start 0))
     (came_from 0)
     (next 0)
    )
  (reconstruct_path_bfs (bfs2 map came_from queue next))
 )
)

(defun bfs2 (map came_from queue next)
  (let 
    (
     (current (car queue))
     (queue2 (cdr queue))
    )
    (cond (is_atom (trace queue))
          (cond (is_atom next)
                (cons came_from 0)
                (bfs2 map came_from next 0)
          )
          (cond (good_cell (map_cell map (car current) (cdr current)))
                (cons came_from current)
                (cond (bad_cell (map_cell map (car current) (cdr current)))
                      (bfs2 map came_from queue2 next)
                      (let
                        (
                         (fresh_neighbors (filter (lambda (x) 
                                                    (is_atom (lookup came_from x))) 
                                                  (neighbor_nodes map current)))
                        )
                        (let
                          (
                           (new_next (l_reduce (lambda (neighbor old_next) 
                                                 (cons neighbor old_next))
                                               fresh_neighbors next))
                           (new_came_from (l_reduce (lambda (neighbor old_came_from)
                                                      (add_to_list
                                                        old_came_from
                                                        neighbor
                                                        current))
                                                    fresh_neighbors came_from))
                           )
                          (bfs2 map new_came_from queue2 new_next)
                         )
                        )
                )
           )
    )
  )
)

(defun reconstruct_path_bfs (came_from_and_target) 
  (
    cond (is_atom (cdr came_from_and_target))
         (cons 0 (cons 0 0))
         (reconstruct_path (car came_from_and_target) (cdr came_from_and_target))
   )
)
