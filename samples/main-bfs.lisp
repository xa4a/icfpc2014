(
(lambda (world_state undoc)
  (let 
    (
     (step (lambda (ai_state world_state)
            (let 
              (
               (map_with_ghosts (overlay_ghosts world_state))
              )
              (let
                (
                 (steps (cdr (reverse (bfs map_with_ghosts 
                                           (get_lman_pos world_state)
                                      )
                             )
                         )
                  )
                 )
                (cons 0 (direction (get_lman_pos world_state)
                                   (car (trace steps))
                        )
                )
              )
             )
            )
     )
    )
    (cons 0 step)
  )
)
#include world.lisp
#include bfs.lisp
)
