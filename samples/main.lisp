(
(lambda (world_state undoc)
  (let 
    (
     (step (lambda (ai_state world_state)
            (let 
              (
               (goal (car (find_goals world_state)))
               (map_with_ghosts (overlay_ghosts world_state))
              )
              (let
                (
                 (steps (cdr (reverse (a-star map_with_ghosts 
                                     (get_lman_pos world_state) 
                                     goal))))
                 )
                (cons 0 (direction (trace (get_lman_pos world_state))
                                   (car (trace steps))
                        )
                )
              )
             )
            )
     )
    )
    (cons 0 step)
  )
)
#include world.lisp
)
