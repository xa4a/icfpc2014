import copy
import logging
import math
import Queue
import random
import sets

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)

LOGGER = logging.getLogger("simulation")

SYMBOL_TO_ELEMENT_DICT = {
    " ": "empty",
    "#": "wall",
    ".": "pill",
    "o": "power_pill",
    "%": "fruit",
    "\\": "lambda_man",
    "=": "ghost"
}

ELEMENT_TO_SYMBOL_DICT = dict((v, k) for (k, v) in SYMBOL_TO_ELEMENT_DICT.items())

DIRECTIONS = ("up", "right", "down", "left")

FRUIT_POINTS = (100, 300, 500, 500, 700, 700, 1000, 1000, 2000, 2000, 3000, 3000, 5000)

GHOST_POINTS = (200, 400, 800, 1600)

GHOST_TICKS = (130, 132, 134, 136)

FRIGHTENED_GHOST_TICKS = (195, 198, 201, 204)

class Maze(object):

    def __init__(self, maze_map, lambda_man_pos, ghost_pos_list):
        assert len(maze_map) > 0

        self.maze_map = maze_map
        self.x = len(maze_map[0])
        self.y = len(maze_map)
        self.level = math.ceil(self.x * self.y / 100.0)

        self.pills = sets.Set()
        self.power_pills = sets.Set()
        for i in xrange(0, self.x):
            for j in xrange(0, self.y):
                if self.get_element_at((i, j)) == "pill":
                    self.pills.add((i, j))
                elif self.get_element_at((i, j)) == "power_pill":
                    self.power_pills.add((i, j))

        self.score = 0
        self.fright_mode = False
        self.fright_mode_start_tick = -1

        self.lambda_man_pos = lambda_man_pos
        self.lambda_man = LambdaMan(self)
        
        self.ghost_pos_dict = {}
        self.ghost_dict = {}
        for i, ghost_pos in enumerate(ghost_pos_list):
            self.ghost_pos_dict[i] = ghost_pos
            self.ghost_dict[i] = Ghost(i, self)

    def valid_coord(self, coord):
        return 0 <= coord[0] < self.x and 0 <= coord[1] < self.y

    def adjacent(self, coord1, coord2):
        return (abs(coord1[0] - coord2[0]) + abs(coord1[1] - coord2[1])) == 1

    def get_direction(self, prev_coord, next_coord):
        #assert self.adjacent(prev_coord, next_coord)
        x_diff = next_coord[0] - prev_coord[0]
        y_diff = next_coord[1] - prev_coord[1]

        if x_diff == 1:
            return "right"
        elif x_diff == -1:
            return "left"
        elif y_diff == 1:
            return "down"
        else:
            return "up"

    def get_opposite_direction(self, direction):
        if direction == "left":
            return "right"
        elif direction == "right":
            return "left"
        elif direction == "up":
            return "down"
        else:
            return "up"

    def get_coord_on_direction(self, coord, direction):
        if direction == "right":
            return (coord[0] + 1, coord[1])
        elif direction == "left":
            return (coord[0] - 1, coord[1])
        elif direction == "down":
            return (coord[0], coord[1] + 1)
        elif direction == "up":
            return (coord[0], coord[1] - 1)
        else:
            return None

    def is_junction(self, coord):
        return len(self.get_free_adjacent_squares(coord)) >= 3

    def get_adjacent_squares(self, coord):
        return filter(lambda x: self.valid_coord(x),
                      ((coord[0] - 1, coord[1]),
                       (coord[0] + 1, coord[1]),
                       (coord[0], coord[1] - 1),
                       (coord[0], coord[1] + 1))
                     )

    def get_free_adjacent_squares(self, coord):
        return filter(lambda x: "wall" != self.get_element_at(x), self.get_adjacent_squares(coord))

    def get_symbol_at(self, coord):
        assert self.valid_coord(coord)
        return maze_map[coord[1]][coord[0]]

    def get_element_at(self, coord):
        return SYMBOL_TO_ELEMENT_DICT[self.get_symbol_at(coord)]

    def set_symbol_at(self, coord, symbol):
        assert symbol in SYMBOL_TO_ELEMENT_DICT
        maze_map[coord[1]][coord[0]] = symbol

    def set_element_at(self, coord, element):
        assert element in ELEMENT_TO_SYMBOL_DICT
        self.set_symbol_at(coord, ELEMENT_TO_SYMBOL_DICT[element])

    def __str__(self):
        square_width = 4 
        rmaze = copy.deepcopy(self.maze_map)  # rendered maze
        x, y = self.lambda_man_pos
        rmaze[y][x] = (ELEMENT_TO_SYMBOL_DICT["lambda_man"] + rmaze[y][x]).strip()

        for ghost_coord in self.ghost_pos_dict.values():
            x, y = ghost_coord
            rmaze[y][x] = (ELEMENT_TO_SYMBOL_DICT["ghost"] + rmaze[y][x]).strip()

        pretty_line = "-" * ((1 + square_width) * self.x + 1)
        result = pretty_line
        result += "\n"
        for maze_row in rmaze:
            #result += "|".join(maze_row) 
            for el in maze_row:
                result += "|"
                result += ("%" + str(square_width) + "s") % el
            result += "|"
            result += "\n"
            result += pretty_line
            result += "\n"

        return result

class LambdaMan(object):

    def __init__(self, maze):
        self.lives = 3
        self.maze = maze
        self.ghosts_eaten = 0

        self.start_pos = self.maze.lambda_man_pos

    def go_home(self):
        self.move_to_no_check(self.start_pos)

    def move_to(self, coord):
        lambda_man_pos = self.maze.lambda_man_pos
        if self.maze.valid_coord(coord) and self.maze.adjacent(coord, lambda_man_pos): 
            if self.maze.get_element_at(coord) == "wall":
                LOGGER.warning("Lambda Man: I'm not Jesus to go into the wall")
            else:
                self.maze.lambda_man_pos = coord
        else:
            LOGGER.warning("Lambda Man: illegal move from " + str(lambda_man_pos) + " to " + str(coord))

    def move_to_no_check(self, coord):
        self.maze.lambda_man_pos = coord

class Ghost(object):

    def __init__(self, index, maze):
        self.index = index
        self.maze = maze

        self.invisible = False
        self.start_pos = self.maze.ghost_pos_dict[index]
        self.start_direction = self.direction = 'down'  # at the beginning, ghosts are face down

    def go_home(self):
        self.move_to_no_check(self.start_pos)
        self.direction = self.start_direction

    def move_to(self, coord):
        ghost_pos = self.maze.ghost_pos_dict[self.index]

        if self.maze.is_junction(ghost_pos):
            is_valid_move = False

            if self.maze.valid_coord(coord) and \
                    self.maze.adjacent(coord, ghost_pos) and \
                    self.maze.get_element_at(coord) != "wall" and \
                    self.direction != self.maze.get_direction(coord, ghost_pos):  # cannot turn back
                is_valid_move = True 

            if is_valid_move:
                self.move_to_no_check(coord)
            else:
                LOGGER.warning("Ghost: illegal move from " + str(ghost_pos) + " to " + str(coord))
                new_coord = self.maze.get_coord_on_direction(ghost_pos, self.direction)
                if self.maze.valid_coord(new_coord) and self.maze.get_element_at(new_coord) != "wall":
                    # can move in the same direction
                    self.move_to_no_check(new_coord)
                else:
                    # need to choose new direction
                    for direction in DIRECTIONS:
                        new_coord = self.maze.get_coord_on_direction(ghost_pos, self.direction)
                        if self.maze.valid_coord(new_coord) and self.maze.get_element_at(new_coord) != "wall":
                            # can move in this direction
                            self.move_to_no_check(new_coord)
                            break

        else:
            possible_moves = self.maze.get_free_adjacent_squares(ghost_pos)
            if len(possible_moves) == 1:  # dead end
                self.move_to_no_check(possible_moves[0])
            else:
                for new_coord in possible_moves:
                    if self.direction != self.maze.get_direction(new_coord, ghost_pos):  # cannot turn back
                        self.move_to_no_check(new_coord)
                        break

    def move_to_no_check(self, coord):
        ghost_pos = self.maze.ghost_pos_dict[self.index]

        self.direction = self.maze.get_direction(ghost_pos, coord)
        self.maze.ghost_pos_dict[self.index] = coord
         

class Event(object):

    def move(self, runner):
        pass

    def action(self, runner):
        pass

    def consume(self, runner):
        pass

    def fight(self, runner):
        pass

    def check_state(self, runner):
        pass

class FruitAppearance(Event):

    def __init__(self, coord):
        self.coord = coord

    def __str__(self):
        return "Fruit appears at " + str(self.coord)

    def action(self, runner):
        maze = runner.maze
        if maze.get_element_at(self.coord) != "wall":
            maze.set_element_at(self.coord, "fruit")

class FruitExpiration(Event):

    def __init__(self, coord):
        self.coord = coord

    def __str__(self):
        return "Fruit expires at " + str(self.coord)

    def action(self, runner):
        maze = runner.maze
        if maze.get_element_at(self.coord) != "wall":
            maze.set_element_at(self.coord, "empty")
 
class EndOfLives(Event):

    def __str__(self):
        return "End of Lives"

    def action(self, runner):
        runner.state = "game_lost"

class LambdaManMove(Event):

    def __str__(self):
        return "Lambda Man move"

    def move(self, runner):
        # example
        curr_pos = runner.maze.lambda_man_pos
        available_moves = runner.maze.get_free_adjacent_squares(curr_pos)
        next_pos = random.choice(available_moves)
        runner.maze.lambda_man.move_to(next_pos)

    def consume(self, runner):
        curr_pos = runner.maze.lambda_man_pos
        element = runner.maze.get_element_at(curr_pos)
        if element == "pill":
            runner.maze.pills.remove(curr_pos)
            runner.maze.set_element_at(curr_pos, "empty")
            runner.score += 10
            runner.event_queue.put((runner.curr_tick + 137, self))
        elif element == "power_pill":
            runner.maze.power_pills.remove(curr_pos)
            runner.maze.set_element_at(curr_pos, "empty")
            runner.score += 50
            runner.event_queue.put((runner.curr_tick + 137, self))

            runner.maze.fright_mode = True
            runner.maze.fright_mode_start_tick = runner.curr_tick
            runner.event_queue.put((runner.curr_tick + 127 * 20, UnsetFrightMode(runner.curr_tick)))
            for g in runner.maze.ghost_dict.values():
                g.direction = runner.maze.get_opposite_direction(g.direction)
        elif element == "fruit":
            runner.maze.set_element_at(curr_pos, "empty")
            fruit_score = FRUIT_POINTS(runner.maze.level - 1) if runner.maze.level <= len(FRUIT_POINTS) else FRUIT_POINTS[-1]
            runner.score += fruit_score 
        else:
            runner.event_queue.put((runner.curr_tick + 127, self))

    def check_state(self, runner):
#        if not runner.maze.pills:
#            runner.state = "game_won"
#            # TODO: add scores for case when all pills are eaten
#        elif runner.maze.lambda_man.lives == 0:
#            runner.state = "game_lost"
        pass

    def fight(self, runner):
        lambda_man_pos = runner.maze.lambda_man_pos
        ghosts_found = filter(lambda x: x == lambda_man_pos, runner.maze.ghost_pos_dict.values())
        if len(ghosts_found) > 0:
            if runner.maze.fright_mode:
                for g in ghosts_found:
                    g.go_home()
                    g.invisible = True
                    runner.maze.lambda_man.ghosts_eaten += 1
                    ghosts_eaten = runner.maze.lambda_man.ghosts_eaten
                    ghost_score = GHOST_POINTS[ghosts_eaten] if ghosts_eaten < len(GHOST_POINTS) else GHOST_POINTS[-1]
                    runner.score += ghost_score
            else:
                runner.maze.lambda_man.lives -= 1
                runner.maze.lambda_man.go_home()
                map(lambda x: x.go_home(), runner.maze.ghost_dict.values())

class GhostMove(Event):

    def __init__(self, ghost):
        self.ghost = ghost

    def __str__(self):
        return "Ghost move"

    def move(self, runner):
        # example
        curr_pos = runner.maze.ghost_pos_dict[self.ghost.index]
        available_moves = runner.maze.get_free_adjacent_squares(curr_pos)
        next_pos = random.choice(available_moves)
        runner.maze.ghost_dict[self.ghost.index].move_to(next_pos)

    def check_state(self, runner):

        if runner.maze.fright_mode:
            wait_ticks = FRIGHTENED_GHOST_TICKS[self.ghost.index % 4]
        else: 
            wait_ticks = GHOST_TICKS[self.ghost.index % 4]

        runner.event_queue.put((runner.curr_tick + wait_ticks, self))

    def fight(self, runner):
        lambda_man_pos = runner.maze.lambda_man_pos
        ghosts_found = filter(lambda x: x == lambda_man_pos, runner.maze.ghost_pos_dict.values())
        if len(ghosts_found) > 0:
            if runner.maze.fright_mode:
                for g in ghosts_found:
                    g.go_home()
                    g.invisible = True
                    runner.maze.lambda_man.ghosts_eaten += 1
                    ghosts_eaten = runner.maze.lambda_man.ghosts_eaten
                    ghost_score = GHOST_POINTS[ghosts_eaten] if ghosts_eaten < len(GHOST_POINTS) else GHOST_POINTS[-1]
                    runner.score += ghost_score
            else:
                runner.maze.lambda_man.lives -= 1
                runner.maze.lambda_man.go_home()
                map(lambda x: x.go_home(), runner.maze.ghost_dict.values())

class UnsetFrightMode(Event):

    def __init__(self, start_tick):
        self.start_tick = start_tick

    def __str__(self):
        return "Unset fright mode"

    def action(self, runner):
        if runner.maze.fright_mode_start_tick == self.start_tick:
            runner.maze.fright_mode = False
            for g in runner.maze.ghost_dict.values():
                g.invisible = False

class Runner(object):

    def __init__(self, maze):
        self.debug = True

        self.state = "running"
        self.score = 0
        self.curr_tick = 0

        self.maze = maze
        self.event_queue = Queue.PriorityQueue()

        eol_tick = 127 * self.maze.x * self.maze.y * 16
        self.event_queue.put((eol_tick, EndOfLives()))

        # TODO: get real fruit coordinates
        self.event_queue.put((127 * 200, FruitAppearance((1, 1))))
        self.event_queue.put((127 * 280, FruitExpiration((1, 1))))
        self.event_queue.put((127 * 400, FruitAppearance((2, 2))))
        self.event_queue.put((127 * 480, FruitExpiration((2, 2))))

        self.event_queue.put((127, LambdaManMove()))

        for index, ghost in self.maze.ghost_dict.items():
            self.event_queue.put((GHOST_TICKS[index % 4], GhostMove(ghost)))

    def run(self):

        while not self.event_queue.empty() and self.state == "running":
            events = []
            self.curr_tick, event = self.event_queue.get()
            events.append(event)
            # get all events with the same tick
            while not self.event_queue.empty():
                tick, e = self.event_queue.get()
                if tick == self.curr_tick:
                    events.append(e)
                else:
                    self.event_queue.put((tick, e))
                    break

            if self.debug:
                print ">> At tick %s: %s, score: %s" % (self.curr_tick, map(lambda x: str(x), events), self.score)

            event.move(self)
            event.action(self)
            event.consume(self)
            #event.fight(self)
            event.check_state(self)
            self.fight()
            self.check_state()

            if self.debug:
                print self.maze

        if self.debug:
            print ">> %s" % self.state 
            print ">> Score: %s" % self.score

    def fight(self):
        lambda_man_pos = self.maze.lambda_man_pos
        ghosts_found = filter(lambda x: self.maze.ghost_pos_dict[x.index] == lambda_man_pos, self.maze.ghost_dict.values())
        if len(ghosts_found) > 0:
            if self.maze.fright_mode:
                for g in ghosts_found:
                    g.go_home()
                    g.invisible = True
                    self.maze.lambda_man.ghosts_eaten += 1
                    ghosts_eaten = self.maze.lambda_man.ghosts_eaten
                    ghost_score = GHOST_POINTS[ghosts_eaten] if ghosts_eaten < len(GHOST_POINTS) else GHOST_POINTS[-1]
                    self.score += ghost_score
            else:
                self.maze.lambda_man.lives -= 1
                if self.debug:
                    print ">> Lives left:", self.maze.lambda_man.lives
                self.maze.lambda_man.go_home()
                map(lambda x: x.go_home(), self.maze.ghost_dict.values())
        
    def check_state(self):
        if not self.maze.pills:
            self.state = "game_won"
            if not runner.maze.power_pills:
                self.score = self.score * (self.maze.lambda_man.lives + 1)
        elif self.maze.lambda_man.lives == 0:
            self.state = "game_lost"

def simple_test():
    maze_map = [
        ["#", " ", " ", "#"],
        ["#", " ", "#", "#"],
        ["#", ".", " ", " "],
        ["#", "#", " ", "#"],
        ["o", " ", " ", " "],
    ]

    lambda_man_pos = (1, 0)
    ghost_pos_list = ((3, 4), (3, 2))

    maze = Maze(maze_map, lambda_man_pos, ghost_pos_list)

    print maze
    maze.lambda_man.move_to((-1, -1))
    maze.lambda_man.move_to((2, 2))

    maze.lambda_man.move_to((1, 1))
    print maze

    maze.lambda_man.move_to((1, 2))
    print maze

    maze.lambda_man.move_to((0, 2))

    print maze.is_junction((2, 4))
    print maze.is_junction((2, 2))
    print maze.is_junction((1, 1))

    ghost1 = maze.ghost_dict[1]

    ghost1.move_to((2, 4))
    print maze

    ghost1.move_to((2, 3))
    print maze

    ghost1.move_to((2, 4))
    print maze

if __name__ == "__main__":

    maze_map = [
        ["#", " ", " ", "#"],
        ["#", "o", "#", "#"],
        ["#", ".", " ", " "],
        ["#", "#", " ", "#"],
        ["o", " ", ".", " "],
    ]

    lambda_man_pos = (1, 0)
    ghost_pos_list = ((3, 4), (3, 2))

    maze = Maze(maze_map, lambda_man_pos, ghost_pos_list)

    runner = Runner(maze)
    runner.run()

