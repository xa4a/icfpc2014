import logging
import sys
import pprint
from collections import namedtuple, deque
from functools import wraps

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  default="/dev/stdin",
                  help="read program from FILE", metavar="FILE")
parser.add_option("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="don't print status messages to stdout")
(options, args) = parser.parse_args()

logging.basicConfig(format='%(message)s')
log = logging.getLogger(__name__)

if options.verbose:
    log.setLevel(logging.DEBUG)
else:
    log.setLevel(logging.INFO)

Closure = namedtuple('Closure', 'f_addr env name')

MAX_CONTROL_STACK_SIZE = 200

TAG_INT, TAG_CONS, TAG_JOIN, TAG_CLOSURE, TAG_STOP, TAG_RET, TAG_DUM= 'INT', 'CONS', 'JOIN', 'CLOSURE', 'STOP', 'RET', 'DUM'

class TAG_MISMATCH(Exception):
    pass

class MACHINE_STOP(Exception):
    pass

def _pop_two_int(method):
    """Decorator that pops two ints from stack and passes them to the method."""
    @wraps(method)
    def new_method(self):
        y = self._POP_s()
        x = self._POP_s()
        if x[0] != TAG_INT or y[0] != TAG_INT:
            raise TAG_MISMATCH()

        return method(self, x[1], y[1])

    return new_method


def decode_list(conss, init):
  current = init
  res = []
  while current != (TAG_INT, 0):
    cons = conss[current[1]]
    res.append(cons[0])
    current = cons[1]

  return res



class TaggedList(list):
    """Just a list subclass with a 'tag' attribute."""
    tag = None

    def __str__(self):
        if self.tag is not None:
            return "%s %s" % (self.tag, super(TaggedList, self).__str__())
        else:
            return super(TaggedList, self).__str__()


class Lman(object):

    def __init__(self):
        self.pc = 0  # Current instruction pointer
        self.data_s = []  # Data stack
        self.control_s = [TAG_STOP]  # Control stack
        self.env_frames = TaggedList()  # Environment frames stack

        self._next_cons = 0  # Id for the next cons() pair.
        self.conss = {}  # Pairs registry.
        self._next_clos = 0  # Id for the next closure to allocate.
        self.closs = {}  # Closures registry.

    def _PUSH_s(self, value):
        """Pushes item onto data stack."""
        self.data_s.insert(0, value)

    def _POP_s(self):
        """Pops item from data stack."""
        return self.data_s.pop(0)

    def _PUSH_d(self, value):
        """Pushes item onto control stack."""
        if len(self.control_s) > MAX_CONTROL_STACK_SIZE:
            raise Exception("control stack overflow")

        self.control_s.insert(0, value)

    def _POP_d(self):
        """Pops item from control stack."""
        return self.control_s.pop(0)

    def _ALLOC_CONS(self, x, y):
        """Allocates a new cons() pair and returns its index (address?)."""
        z = self._next_cons
        self.conss[z] = (x, y)
        self._next_cons += 1
        return z

    def _ALLOC_CLOSURE(self, f, env, name):
        """Allocates a new closure and returns its index."""

        self.closs[self._next_clos] = Closure(f, env, name)
        self._next_clos += 1
        return self._next_clos - 1

    def _ALLOC_FRAME(self, n):
        """Allocates a new frame in the current env. stack."""
        return TaggedList([TaggedList([None]*n)] + self.env_frames)

    def LDC(self, const):
        """Pushes constant onto stack."""
        self._PUSH_s((TAG_INT, const))
        self.pc += 1

    def LD(self, frame_n, elem_i):
        """Pushes value from environment."""
        frame = self.env_frames[frame_n]
        if elem_i >= len(frame):
          raise Exception("Invalid LD reference. Var on wrong level? Invalid number of args?")
        self._PUSH_s(frame[elem_i])
        self.pc += 1

    @_pop_two_int
    def ADD(self, x, y):
        self.LDC(x+y)

    @_pop_two_int
    def SUB(self, x, y):
        self.LDC(x-y)

    @_pop_two_int
    def MUL(self, x, y):
        self.LDC(x*y)

    @_pop_two_int
    def DIV(self, x, y):
        self.LDC(x/y)

    @_pop_two_int
    def CEQ(self, x ,y):
        self.LDC(int(x == y))

    @_pop_two_int
    def CGT(self, x ,y):
        self.LDC(int(x > y))

    @_pop_two_int
    def CGTE(self, x ,y):
        self.LDC(int(x >= y))

    def ATOM(self):
        """Checks whether top element is int."""
        x = self._POP_s()
        self.LDC(x[0] == TAG_INT)

    def CONS(self):
        y = self._POP_s()
        x = self._POP_s()

        z = self._ALLOC_CONS(x, y)
        self._PUSH_s((TAG_CONS, z))
        self.pc += 1

    def CAR(self):
        x = self._POP_s()
        if x[0] != TAG_CONS:
            raise TAG_MISMATCH(x)
        self._PUSH_s(self.conss[x[1]][0])
        self.pc += 1

    def CDR(self):
        x = self._POP_s()
        if x[0] != TAG_CONS:
            raise TAG_MISMATCH
        self._PUSH_s(self.conss[x[1]][1])
        self.pc += 1

    def SEL(self, t, f, tail=False):
        x = self._POP_s()
        if x[0] != TAG_INT:
            raise TAG_MISMATCH

        if not tail:
            self._PUSH_d((TAG_JOIN, self.pc+1))
        if x[1] == 0:
            self.pc = f
        else:
            self.pc = t

    def JOIN(self):
        x = self._POP_d()
        if x[0] != TAG_JOIN:
            raise TAG_MISMATCH

        self.pc = x[1]

    def LDF(self, f, fname):
        """Allocates a new closure/defines a function with current env."""
        x = self._ALLOC_CLOSURE(f, TaggedList(self.env_frames[:]), fname)
        self._PUSH_s((TAG_CLOSURE, x))
        self.pc += 1

    def AP(self, n_args, tail=False):
        """Invokes function at top of stack with n_args args from stack."""
        x = self._POP_s()
        if x[0] != TAG_CLOSURE:
          msg = "%s got tag %s, expected %s, i.e. tried to call not a function. extra() somewhere?" % (x, x[0], TAG_CLOSURE)
          raise TAG_MISMATCH(msg)

        closure = self.closs[x[1]]
        log.debug('APing %s', closure.name)
        f = closure[0]
        e = closure[1]
        fp = self._ALLOC_FRAME(n_args)

        fp = fp[0:1] + e
        for i in range(n_args-1, -1, -1):
            fp[0][i] = self._POP_s()

        if not tail:
            self._PUSH_d(TaggedList(self.env_frames[:]))    # %d := PUSH(%e,%d); save frame pointer
            self._PUSH_d((TAG_RET, self.pc + 1))
        self.env_frames = fp
        self.pc = f

    def RTN(self):
        """Returns from a function, restores prev env and PC."""
        x = self._POP_d()
        if x == TAG_STOP:
            raise MACHINE_STOP()
        if x[0] != TAG_RET:
            raise TAG_MISMATCH('control mismatch')

        self.env_frames = self._POP_d()
        self.pc = x[1]

    def DUM(self, n):
        """Allocates a dummy env. frame."""
        self.env_frames = self._ALLOC_FRAME(n)
        self.env_frames[0].tag = TAG_DUM
        self.pc += 1

    def RAP(self, n, tail=False):
        """Recursive application(?)."""
        x = self._POP_s()
        if x[0] != TAG_CLOSURE:
            raise TAG_MISMATCH()

        f, fp, fname = self.closs[x[1]]

        log.debug('RAPing %s', fname)

        if self.env_frames[0].tag != TAG_DUM:
            raise TAG_MISMATCH('FRAME_MISMATCH')

        if len(self.env_frames[0]) != n:
            raise TAG_MISMATCH('frame mismatch')

        for i in range(n-1, -1, -1):
            fp[0][i] = self._POP_s()

        fp.tag = ''
        if not tail:
            self._PUSH_d(TaggedList(self.env_frames[1:]))    # save frame pointer
            self._PUSH_d((TAG_RET, self.pc + 1))    # save return address
        self.env_frames = fp
        self.pc = f

    def STOP(self):
        raise MACHINE_STOP()

    # Tail call extensions
    def TSEL(self, t, f):
        self.SEL(t, f, tail=True)

    def TAP(self, n):
        self.AP(n, tail=True)

    def TRAP(self, n):
        self.RAP(n, tail=True)

    def ST(self, n, i):
        fp = self.env_frames[n]
        if fp.tag == TAG_DUM:
            raise TAG_MISMATCH('frame mismatch')
        v = self._POP_s()
        fp[i] = v
        self.pc += 1

    def DBUG(self):
        x = self._POP_s()
        print 'trace:',
        if x[0] == TAG_INT:
          print x
        elif x[0] == TAG_CONS:
          print decode_list(self.conss, x)
        self.pc += 1

    def BRK(self):
        print 'BRK'
        self.pc += 1

    def exec_gcc(self, lines):
      instructions = []
      labels = {}
      for l in parse(lines):
          #log.debug('parsed line: %s', l)
          if l[0] == 'label':
              name, addr = l[1]
              labels[name] = addr
          elif l[0] == 'instr':
              instructions.append(l[1])

      log.debug('labels: %s', labels)
      latest_commands = deque(maxlen=20)
      unary_int = frozenset(['LDC', 'AP', 'DUM', 'RAP', 'TRAP', 'TAP'])
      noarg = frozenset(['RTN', 'ADD', 'SUB', 'MUL', 'DIV', 'LDC', 'JOIN', 'CONS', 'CEQ', 'CAR', 'CDR', 'ATOM', 'CGT', 'CGTE', 'DBUG', 'BRK'])
      try:
          while True:
              i = instructions[self.pc]
              latest_commands.append(i)
              log.debug('executing[%d]: %s', self.pc, i)

              if i[0] == 'LDF':
                  if i[1] in labels:
                      addr = labels[i[1]]
                      name = i[1]
                  else:
                      addr = int(i[1])
                      name = '<>'
                  self.LDF(addr, name)

              elif i[0] in ['TSEL', 'SEL']:
                  if i[1] in labels:
                      t_addr = labels[i[1]]
                  else:
                      t_addr = int(i[1])

                  if i[2] in labels:
                      f_addr = labels[i[2]]
                  else:
                      f_addr = int(i[2])
                  if i[0] == 'SEL':
                    func = self.SEL
                  else:
                    func = self.TSEL
                  func(t_addr, f_addr)

              elif i[0] in unary_int:
                  getattr(self, i[0])(int(i[1]))

              elif i[0] in noarg:
                  getattr(self, i[0])()

              elif i[0] == 'LD':
                  self.LD(int(i[1]), int(i[2]))

              else:
                  raise Exception('instruction not implemented: %s' % i)

              log.debug('done')
              log.debug('data stack: %s', pprint.pformat(self.data_s))
              log.debug('closures stack: %s', pprint.pformat(self.closs))
              log.debug('env frames: %s', pprint.pformat(self.env_frames))
              log.debug('allocated pairs: %s', pprint.pformat(self.conss))
              log.debug('control stack: %s', pprint.pformat(self.control_s))
              log.debug('--------')
      except MACHINE_STOP:
          pass
      except:
        print 'latest commands:'
        for i in latest_commands:
          if ';' in i:
            print i[:i.index(';')], ' '.join(i[i.index(';'):])
          else:
            print i
        raise

      return self.data_s


def parse(lines):
    addr = 0

    for line in lines:
        #log.debug('parsing line: %s', line.strip())
        line = line.strip()
        if not line:
            continue
        #parts = line.split(';', 1)
        #if len(parts) > 1:
        #    log.debug('comment %s', parts[1])
        parts = [line]
        if ':' in parts[0]:
            yield ('label', (parts[0].partition(':')[0].strip(), addr))
        else:
            yield ('instr', parts[0].split())

            addr += 1


def main(argv):
    lines = open(options.filename).readlines()

    print Lman().exec_gcc(lines)


if __name__ == '__main__':
    main(sys.argv)
