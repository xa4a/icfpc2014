import logging
import sys

REG, REG_IND, CONST, MEM = ('reg', 'reg_ind', 'const', 'mem')

log = logging.getLogger(__name__)

log.setLevel(logging.INFO)
log.addHandler(logging.FileHandler('/dev/stderr'))

class GHC(object):
  pc = 0

  def __init__(self):
    pass

  def load_program(self, program):
    self.program = program



def parse_arg(arg):
  arg = arg.strip()
  log.debug('parsing arg: %s', arg)
  if arg in 'abcdefgh' or arg.upper() == 'PC':
    return (REG, arg.upper())

  try:
    return (CONST, int(arg))
  except ValueError:
    pass

  assert arg[0] == '[' and arg[-1] == ']'

  arg = arg[1:-1]

  if arg in 'abcdefgh':
    return (REG_IND, arg.upper())

  return (MEM, int(arg))

def parse_ghc(lines):
  for line in lines:
    log.debug('parsing: %s', line.strip())
    line_and_comment = line.split(';', 1)
    line = line_and_comment[0].strip()
    if len(line_and_comment) > 1:
      log.info('comment: %s', ''.join(line_and_comment[1:]).strip())
    if not line:
      continue
    parts = line.split()
    instruction = parts[0].upper()
    if len(parts) > 1:
      args = map(parse_arg, parts[1].split(','))
    else:
      args = []
    log.debug('instr: %s, args: %s', instruction, args)
    yield tuple([instruction, args])



def main(argv):
  lines = open(argv[1]).readlines()

  for instruction in parse_ghc(lines):
    print '>',instruction


if __name__ == '__main__':
  main(sys.argv)
