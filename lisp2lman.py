"""Sample invocation: python lisp2lman.py -f samples/sexp.lisp."""
import collections
import re
import logging
from optparse import OptionParser

import sexpparser

INDENT = '  '


logging.basicConfig(format='%(message)s')
log = logging.getLogger(__name__)

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  default="/dev/stdin",
                  help="read program from FILE", metavar="FILE")
parser.add_option("-s", "--step",
                  action="store_true", dest="exec_step", default=False,
                  help="don't print status messages to stdout")
(options, args) = parser.parse_args()

def encode(struct):
    if isinstance(struct, int) or isinstance(struct, basestring) and len(struct) == 1:
        return str(struct)
    elif isinstance(struct, (list, basestring)):
        acc = '0'
        for elem in struct[::-1]:
            acc = "(cons %s %s)" % (encode(elem), acc)
        return acc
    elif isinstance(struct, tuple):
        assert len(struct) > 1
        acc = encode(struct[-1])
        for elem in struct[-2::-1]:
            acc = "(cons %s %s)" % (encode(elem), acc)
        return acc


Function = collections.namedtuple("Function", "name contents")

class Addr(object):
    def __init__(self, label):
        self.label = label

    def __repr__(self):
        return "<Addr: %s>" % self.label

class Instruction(list):
    addr = None

    def __repr__(self):
        if self.addr is not None:
            return "%s: %s" % (self.addr, super(Instruction, self).__repr__())
        else:
            return super(Instruction, self).__repr__()

class LetVar(object):
    def __init__(self, addr, pos):
        self.addr = addr
        self.pos = pos

I = Instruction

class Environment(object):
    def __init__(self):
        self.branches = {}
        self.branch_addrs = {}
        self.functions = {}
        self.func_addrs = {}
        self.vars = [{}]

    def lookup_var(self, var_name):
        for level, env in enumerate(self.vars):
            if var_name in env:
                return (level, env[var_name])
        return None

    def lookup_func(self, func_name):
        return self.func_addrs[func_name]

    def lookup_branch(self, br_name):
        return self.branch_addrs.get(br_name)

    def push_env_frame(self):
        self.vars.insert(0, {})

    def pop_env_frame(self):
        self.vars.pop(0)

def trampoline(env, exec_step=options.exec_step):
    """exec_step - exec step function after main returns"""
    func_defs = env.functions.values()
    content_len = sum(len(f.contents) for f in func_defs)
    header_len = 2
    header = [
      ["LDC", 42],
      ["TSEL", header_len + content_len, 0],
    ]

    footer = []
    addr = header_len
    for func_def in func_defs:
        env.func_addrs[func_def.name] = addr
        #footer.append(["LDF", addr, '\t; decl ' + func_def.name])
        addr += len(func_def.contents)

    footer.append(['LDC', 1])
    footer.append(['LDC', 1])

    footer.extend([
        ["LDF", env.func_addrs['main'], '\t; prepare main to be called'],
        ["AP", 2],
    ])
    if exec_step:
        footer.extend([
            ["LDF", env.func_addrs['step_adapter'], '\t; prepare step to be called'],
            ["AP", 2],
        ])
    footer.append(["RTN", '\t\t ; final RTN\n'])
    footer[0].append('\t; trampoline')
    return header, footer


_SEL_ID = 0


def t(exp, env):
    """Returns list of (INSTRUCTION, ARG1, ARG2)."""
    global _SEL_ID
    if isinstance(exp, basestring):
        lookup = env.lookup_var(exp)
        if lookup is not None:
            if isinstance(lookup[1], LetVar):
                return [
                        ["LD", lookup[0], lookup[1].pos, '\t; prepare let var %s' % exp],
                        #["LDF", lookup[1].addr, '\t; prepare let var %s' % exp],
                        ["AP", 0, "\t; extract let var %s" % exp]]
            return [
                    ['LD', lookup[0], lookup[1], '\t; loading var', exp],
                    #["AP", 0, "\t; extract let var %s" % exp],
                    ]
        else: # Not a let binding, must be upcoming global.
            f_addr = env.functions.setdefault(exp, Addr(exp))
            return [
                    ["LDF", f_addr, '\t; call func %s' % exp],
            ]



    binary_ops = {
            '+': 'ADD',
            '-': 'SUB',
            '*': 'MUL',
            '/': 'DIV',
            '=': 'CEQ',
            '>': 'CGT',
            '>=': 'CGTE',
    }
    # Ops with args reversed
    rev_binary_ops = {
            '<': 'CGT',
            '<=': 'CGTE',
    }

    if isinstance(exp, int):
        return [['LDC', exp]]

    if isinstance(exp[0], basestring):
        # define global functions
        if exp[0] == 'defun':
            f_name = exp[1]
            args_names = exp[2]
            local_env = env.vars[0]
            for i, name in enumerate(args_names):
                local_env[name] = i
            body = t(exp[3].asList(), env)
            body[0] = I(body[0] + ['\t; def %s' % f_name])
            body[0].addr = Addr(f_name)
            body.append(
                ['RTN',]# '\t\t; ret from ' + f_name]
            )
            return body

        elif exp[0] in binary_ops:
            assert len(exp) == 3, '%s operation expected three args, got just: %s' % (exp[0], exp)
            arg1 = t(exp[1], env)
            arg2 = t(exp[2], env)
            return arg1 + arg2 + [[binary_ops[exp[0]]]]
        elif exp[0] in rev_binary_ops:
            assert len(exp) == 3
            arg1 = t(exp[1], env)
            arg2 = t(exp[2], env)
            return arg2 + arg1 + [[rev_binary_ops[exp[0]]]]
        elif exp[0] == 'cons':
            assert len(exp) == 3, "bad cons: %s" % exp
            arg1 = t(exp[1], env)
            arg2 = t(exp[2], env)
            return arg1 + arg2 + [['CONS']]
        elif exp[0] == 'car':
            assert len(exp) == 2, "Wrong car: %s" % exp
            arg = t(exp[1], env)
            return arg + [["CAR"]]
        elif exp[0] == 'cdr':
            assert len(exp) == 2
            arg = t(exp[1], env)
            return arg + [["CDR"]]
        elif exp[0] == 'cond':
            assert len(exp) == 4, "cond invocation invalid: %s" % exp
            cond = t(exp[1], env)
            select_id = _SEL_ID
            _SEL_ID += 1

            true_name = "%d_true" % select_id
            true = t(exp[2], env) + [['JOIN']]
            true[0] = I(true[0] + ['\t; ' + true_name])
            true[0].addr = Addr(true_name)

            false_name = "%d_false" % select_id
            false = t(exp[3], env) + [['JOIN']]
            false[0] = I(false[0] + ['\t; ' + false_name])
            false[0].addr = Addr(false_name)

            cond_sel = I(["SEL", true[0].addr, false[0].addr, '\t; cond'])
            cond_sel.addr = Addr('cond')
            return cond + [
                    ["LDC", 42],
                    ["TSEL", cond_sel.addr, 0, "\t; skip branches to condition"],
            ] + true + false + [cond_sel]
        elif exp[0] == 'is_atom':
            return t(exp[1], env) + [["ATOM"]]
        elif exp[0] == 'let':
            assert len(exp) == 3, "bad let: %s" % exp
            var_defs = exp[1]
            value = exp[2]

            var_decls = []
            var_bodies = []
            env.push_env_frame()
            for var_pos, var_def in enumerate(var_defs):
                var_name = var_def[0]

                # Declare var in current scope early to allow recursion.
                var_addr = Addr('let %s' % var_name)
                env.vars[0][var_name] = LetVar(var_addr, var_pos)

                var_value = t(var_def[1], env)
                var_value[0] = I(var_value[0] + ['\t; let %s' % var_name])
                var_value[0].addr = var_addr
                var_value.append(['RTN', '\t\t; ret from let'])

                var_decls.append(['LDF', var_value[0].addr, '\t; let var %s entry point'% var_name])
                var_bodies.extend(var_value)

            let_names = [x[0] for x in var_defs]

            # Body is executed inside its RAP with all let bindings.
            body = t(value, env)
            env.pop_env_frame()
            body[0] = I(body[0] + ['\t; let (%s) body, after decls' % let_names])
            body[0].addr = Addr("let body")


            let_initializer = [
                    #I(["DUM", len(var_decls), "\t; frame for let decls"]),
                    I(["LDF", body[0].addr, '\t; prepare let (%s) body' % let_names]),
                    ["TAP", len(var_decls), '\t; exec let body with (%s) let bindings' % let_names],
            ]
            let_initializer[0].addr = Addr('foo')


            res = var_decls + [
                   ["LDC", 42],
                   ["TSEL", let_initializer[0].addr, 0, '\t; skip let var bodies (%s) to let init' % let_names],
            ] + var_bodies + let_initializer + body

            return res
        #elif exp[0] == 'func_ref':
        #    # Try lookup the func as a var.
        #    var = env.lookup_var(exp[1])
        #    if var is not None:
        #        return [["LD", var[0], var[1]]]
        #    else:
        #        return [["LDF", exp[1]]]

        elif exp[0] == 'trace':
            return t(exp[1], env) + t(exp[1], env) + [["DBUG"]]

        elif exp[0] == 'brk':
            return [["BRK"]] + t(exp[1], env)

        elif exp[0] == 'probe':
            return [["LDC", exp[1]],["DBUG"]] + t(exp[1], env) + [["LDC", exp[1]+1],["DBUG"]]

        elif exp[0] == 'lambda':
            assert len(exp) == 3, 'wrong lambda def: %s' % exp
            args_names = exp[1]
            env.push_env_frame()
            local_env = env.vars[0]
            for i, name in enumerate(args_names):
                local_env[name] = i
            body = t(exp[2], env)
            env.pop_env_frame()

            body[0] = I(body[0] + ['\t; lambda (%s) body' % args_names])
            body.append(['RTN', '\t\t; ret from lambda'])

            lambda_addr = Addr('lambda')
            body[0].addr = lambda_addr
            after_lambda_addr = Addr('skip_lambda')

            res = [
                    ["LDC", 42],
                    ["TSEL", after_lambda_addr, 0, '\t; skip lambda body'],
            ] + body
            ldf_lambda = I(["LDF", lambda_addr, "\t; load lambda (%s)" % args_names])
            ldf_lambda.addr = after_lambda_addr
            res.append(ldf_lambda)
            return res

        #if isinstance(exp, basestring): # just a func name (from let). apply it.
        #    lookup = env.lookup_var(exp)
        #    if lookup is not None:
        #        if isinstance(lookup[1], LetVar):
        #            return [
        #                    #["LDF", lookup[1].addr, '\t; prepare let var %s' % exp],
        #                    ["LD", lookup[0], lookup[1].pos, '\t; prepare let var %s' % exp],
        #                    ["AP", 0, "\t; extract let var %s" % exp],
        #            ]
        #        else:
        #            raise Exception()


        # Func application.
        res = []
        f_name = exp[0]
        f_args = exp[1:]

        for arg in f_args:
            res.extend(t(arg, env))
        if f_name not in env.functions:
            # Extract closure from name
            res.extend(t(f_name, env))
        else:
            res.append(["LDF", env.functions[f_name], '\t; loading global function', f_name])

        # Apply closure
        res.extend([
            ["AP", len(f_args), "\t; apply %s" % f_name],
        ])

        # Try lookup the func as a var.
        #var = env.lookup_var(f_name)
        #if var is not None:
        #    if isinstance(var[1], LetVar):
        #        res.extend([
        #            ["DUM", 0],
        #            ["LDF", var[1].addr, '\t; prepare let var %s' % f_name],
        #            ["RAP", 0, '\t\t; fetch letvar %s' % f_name],
        #        ])

        #    else:
        #        res.append(["LD", var[0], var[1]])
        #else:
        #    raise
        #res.append(["AP", len(f_args), "\t\t; call %s" % f_name])
        return res
    else:
        # exp[0] is an expression that returns (puts on stack) a closure.
        # Put its args on stack first, then the closure, then AP itself.
        res = []
        for arg in exp[1:]:
            res.extend(t(arg, env))
        res.extend(t(exp[0], env))
        res.append(["AP", len(exp[1:]), "\t\t; apply computed closure %s" % exp])
        return res


def trampoline2(init_addr):
    return [
            ["LDF", init_addr, "\t; load init"],
            ["TAP", 0],
    ]

def transform_top(s):
    try:
        exp = sexpparser.sexp.parseString(s)
    except sexpparser.ParseException as e:
        print 'Parse error near line: %d col: %d' % (e.lineno, e.col)
        context_lines = 5
        lines = s.splitlines()
        for l in lines[max(0, e.lineno-context_lines):e.lineno+1]:
            print l
        print '-' * (e.col-1) + '^'
        for l in lines[e.lineno+1:e.lineno+context_lines]:
            print l
        raise

    env = Environment()

    global_funcs = []
    for global_func in exp[0][1:]:
        assert global_func[0] == 'defun'
        f_name = global_func[1]
        addr = env.functions.setdefault(f_name, Addr(f_name))
        body = t(global_func, env)
        body[0] = I(body[0])
        body[0].addr = addr
        #footer.append(["LDF", addr, '\t; decl ' + func_def.name])
        global_funcs.extend(body)


    top_level_fun = t(exp[0][0], env)
    top_level_fun[0] = I(top_level_fun[0])
    top_level_fun[0].addr = Addr('entry point')
    top_level_fun.append(["AP", 2, "\t\t; apply main to argv"])

    trampoline = trampoline2(top_level_fun[0].addr)

    #tramp_hdr, tramp_footer = trampoline(top_level_fun, env)

    emulated_argv = [
            ["LDC", 1],
            ["LDC", 1],
    ]

    #result = tramp_hdr + top_level_fun + tramp_footer
    result = emulated_argv + trampoline + top_level_fun + [['RTN']] + global_funcs
    #addr = len(result)
    #for br_name, branch in env.branches.items():
    #    env.branch_addrs[br_name] = addr
    #    result += branch
    #    result[addr] += ['\t; branch %s' % br_name]
    #    addr += len(branch)
    return env, result


def render(env, instructions):
    res = []
    notable_addrs = {}
    for physical_addr, i in enumerate(instructions):
        if getattr(i, 'addr', None) is not None:
            notable_addrs[i.addr] = physical_addr

    for instr in instructions:
        if instr[0] == 'LDF':
            if isinstance(instr[1], Addr):
                instr[1] = notable_addrs[instr[1]]
        elif instr[0] in ['SEL', 'TSEL']:
            if isinstance(instr[1], Addr):
                instr[1] = notable_addrs[instr[1]]
            if isinstance(instr[2], Addr):
                instr[2] = notable_addrs[instr[2]]
        line = ' '.join(map(str, instr)) + '\n'
        if ':' not in line:
            line = INDENT + line
        res.append(line)

    return ''.join(res)

def lisp2lman(lisp_text):
    include_regexp = re.compile(r'#include (.*?\.lisp)')
    m = include_regexp.search(lisp_text)
    while m:
        lisp_text = lisp_text.replace(m.group(0), open('samples/'+m.group(1)).read())
        m = include_regexp.search(lisp_text)

    #print lisp_text
    env, instructions = transform_top(lisp_text)
    return render(env, instructions)

def main():
    s = open(options.filename).read()
    print lisp2lman(s)

if __name__ == '__main__':
    main()
